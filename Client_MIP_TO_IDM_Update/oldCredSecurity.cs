﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Client_MIP_TO_IDM_Update
{
    public class oldCredSecurity
    {
        public static bool Login(string username, string password)
        {
            Models.APISecurityCRMEntities entities;

            using (entities = new Models.APISecurityCRMEntities())
            {
                return entities.CredUsers.Any(user =>
                       user.Username.Equals(username, StringComparison.OrdinalIgnoreCase)
                                          && user.Password == password);
            }
        }
    }
}