﻿using System.Web;
using System.Web.Mvc;

namespace Client_MIP_TO_IDM_Update
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
