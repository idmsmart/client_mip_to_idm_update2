﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Client_MIP_TO_IDM_Update
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            // New code
            config.EnableCors();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/x-www-form-urlencoded"));

            //FormUrlEncodedMediaTypeFormatter f;
            //config.Formatters.Clear();
            ////config.Formatters.Add(new JsonMediaTypeFormatter());

            //f = new FormUrlEncodedMediaTypeFormatter();
            ////f.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            ////f.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            //f.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/x-www-form-urlencoded"));

            //config.Formatters.Add(f);
            ////application/x-www-form-urlencoded ???
            //config.Formatters.FormUrlEncodedFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/x-www-form-urlencoded"));



        }
    }
}
