﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDHCL2MIP;
using CDHCL2MIP.JsonBuilder;
using Client_MIP_TO_IDM_Update.MIP2CRM;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using log4net;

namespace Client_MIP_TO_IDM_Update.Controllers
{
    public class RetrieveCRMdataController : ApiController
    {
        MIP_TO_IDM_ClientUpdateEntities mIP_TO_IDM = new MIP_TO_IDM_ClientUpdateEntities();
        ProcessCDHClient CDHClient = new ProcessCDHClient();
        private JavaScriptSerializer js = new JavaScriptSerializer();
        ProcessDBToMipclient db_mip = new ProcessDBToMipclient();
        ILog log = LogManager.GetLogger(typeof(RetrieveCRMdataController));
        [Route("GetSpecifiedClient")]
        
        [BasicAuthentication]
        public object GetByID(string Clientidnumber)
        {
            ILog log = LogManager.GetLogger(typeof(RetrieveCRMdataController));
            try
            {
                log.Info(Clientidnumber);
                if (Clientidnumber == null) return Json(new { Message = "No record found" });
                var oppos = mIP_TO_IDM.usp_SelectOppIDforMIP(Clientidnumber).FirstOrDefault();
                if (oppos == null)
                {
                    log.Info("MIP call no results on oppo " + DateTime.Now);
                    return Json(new { Message = "No record found" });
                }
                else
                {
                    log.Info("MIP call entry with ID " + DateTime.Now);
                    string json_res = CDHClient.StartingMainCode1(Convert.ToInt32(oppos));

                    if (json_res != null)
                    {
                        Root Jsonresponse = JsonConvert.DeserializeObject<Root>(json_res);
                        log.Info("MIP call response " + DateTime.Now);
                        return Jsonresponse;
                    }
                    else
                    {
                        log.Info("MIP call no results " + DateTime.Now);
                        return Json(new { Message = "No record found" });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("MIP call entry " + DateTime.Now + " " + ex.Message.ToString());
                return Json(new { Message = "No record found" });
            }
        }


        [Route("RetrieveClients")]
        // [BasicAuthentication]
        public object Get()
        {
            try
            {
                log.Info("MIP call entry " + DateTime.Now);

                List<int> oppos_list = new List<int>();

                var oppos_check = mIP_TO_IDM.GetListofQueuedOppos().FirstOrDefault();
                if (oppos_check != null)
                {
                    List<GetListofQueuedOppos_Result> oppos = GetOppiLists();

                    foreach (var oppids in oppos)
                    {
                        oppos_list.Add((int)oppids.OppoID);
                    }
                    log.Info("MIP call before Json build " + DateTime.Now);
                    log.Info("Unmarking Oppos " + DateTime.Now);
                    foreach (var oppids in oppos)
                    {
                        mIP_TO_IDM.unQueueOppo((int)oppids.OppoID);
                    }
                    log.Info("Unmarking Oppos Done " + DateTime.Now);
                    string json_res = db_mip.StartingMainCode_ForBulkFile(oppos_list);
                    log.Info(json_res);
                    if (json_res != null)
                    {
                        Root Jsonresponse = JsonConvert.DeserializeObject<Root>(json_res);
                        log.Info(Jsonresponse);
                        log.Info("MIP Retuning full json results " + DateTime.Now);
                        return Jsonresponse;
                    }
                    else
                    {
                        log.Info("MIP Retuning json null " + DateTime.Now);
                        return Json(new { Message = "No record found" });
                    }
                }
                else
                {
                    log.Info("DB dont have Oppos  null " + DateTime.Now);
                    return Json(new { Message = "No record found" });
                }
            }
            catch (Exception ex)
            {
                log.Info("MIP Retuning fail to try " + DateTime.Now);
                log.Info("MIP Retuning fail to try " + ex.Message.ToString());
                return Json(new { Message = "No record found" });
            }
        }

        private List<GetListofQueuedOppos_Result> GetOppiLists()
        {
            return mIP_TO_IDM.GetListofQueuedOppos().ToList();
        }
    }
}
