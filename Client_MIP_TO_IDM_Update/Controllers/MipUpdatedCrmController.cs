﻿using Client_MIP_TO_IDM_Update.MIpmodels;
using System;
using System.Data.Entity.Validation;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml.Linq;
using Newtonsoft.Json;
using log4net;
using System.Web.Script.Serialization;
using System.Linq;
using Client_MIP_TO_IDM_Update.MIP2CRM;
using System.Collections.Generic;

namespace Client_MIP_TO_IDM_Update.Controllers
{
    public class MipUpdatedCrmController : ApiController
    {

        private JavaScriptSerializer js = new JavaScriptSerializer();
        MIP_TO_IDM_ClientUpdateEntities _mipcontext;//= new MIP_TO_IDM_ClientUpdateEntities();
        [Route("MipUpdatedCrmResults")]
        [BasicAuthentication]
        //[HttpPut]
        [HttpPost]
        public async Task<IHttpActionResult> MIP_TO_IDM(clientupdate clientupdates)
        {
            string jsonData = js.Serialize(clientupdates);
            ILog log = LogManager.GetLogger(typeof(MipUpdatedCrmController));
            log.Info(jsonData);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                foreach (dynamic updateclient in clientupdates.L_Clients)
                {
                        using (var Acklg_Executor = new MIP_TO_IDM_ClientUpdateEntities())
                        {

                            var TbAlkknoladge = new MIPTOCRM
                            {
                                ClientIDnumber = updateclient.ClientIDNumber,
                                ClientLeadID = Convert.ToInt32(updateclient.ClientLeadId),
                                InsertionTime = DateTime.Now,
                                UpdateStatus = updateclient.ClientUpdateStatus,
                                UniqueRefences = updateclient.UniqueRefences
                                
                            };

                            Acklg_Executor.MIPTOCRMs.Add(TbAlkknoladge);
                            var res = await Acklg_Executor.SaveChangesAsync();
                        }
                }
                return Json(new { Message = "success" });
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {

                    foreach (var ve in eve.ValidationErrors)
                    {

                        return Json(new { Message = "No success" });
                    }
                }
                return Json(new { Message = "No success" });
            }
        }
    }
}
