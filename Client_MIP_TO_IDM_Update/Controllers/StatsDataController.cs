﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using MassTransit;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.Mvc;
//using System.Web.Http.Controllers;
//using System.Configuration;
//using System.Data.SqlClient;
//using log4net;


////new webhook for leadify

//namespace Client_MIP_TO_IDM_Update.Controllers
//{
//    public class StatsDataController : ApiController
//    {
//        ILog log = LogManager.GetLogger(typeof(StatsDataController)); 
//        #region PushLeadStats
//        [System.Web.Http.Route("IMDLeadStats")]
//        //public HttpResponseMessage Post(AllLeadStats datarecord)
//        public async Task<ActionResult> Post(AllLeadStats datarecord )
//        {
//            log.Info("datarecord  =  " + datarecord + "campaignid = " + datarecord.campaignid);
//            string constring;
//            bool test = false;
 
//            if (datarecord.campaignid == null)
//            {
//                datarecord.campaignid = "campaignid";
//            }
//            #region checking null fields
//            if (datarecord.webhooktype == null)
//            {
//                datarecord.webhooktype = "webhooktype";
//            }
//            if (datarecord.eventdate == null)
//            {
//                datarecord.eventdate = "1900-00-20T14:03:23";
//            }
//            if (datarecord.campaignname == null)
//            {
//                datarecord.campaignname = "campaignname";
//            }
//            if (datarecord.campaigncategory == null)
//            {
//                datarecord.campaigncategory = "campaigncategory";
//            }
//            if (datarecord.ipaddress == null)
//            {
//                datarecord.ipaddress = "ipaddress";
//            }
//            if (datarecord.useragent == null)
//            {
//                datarecord.useragent = "useragents";
//            }
//            if (datarecord.cellphone == null)
//            {
//                datarecord.cellphone = "cellphone";
//            }
//            if (datarecord.firstname == null)
//            {
//                datarecord.firstname = "firstname";
//            }
//            if (datarecord.lastname == null)
//            {
//                datarecord.lastname = "lastname";
//            }
//            if (datarecord.email == null)
//            {
//                datarecord.email = "email";
//            }
//            if (datarecord.gender == null)
//            {
//                datarecord.gender = "gender";
//            }
//            if (datarecord.idnumber == null)
//            {
//                datarecord.idnumber = "idnumber";
//            }
//            if (datarecord.race == null)
//            {
//                datarecord.race = "race";
//            }
//            if (datarecord.dateofbirth == null)
//            {
//                datarecord.dateofbirth = "1900-00-20T14:03:23";
//            }
//            if (datarecord.region == null)
//            {
//                datarecord.region = "region";
//            }
//            if (datarecord.custom1 == null)
//            {
//                datarecord.custom1 = "custom1";
//            }
//            if (datarecord.custom2 == null)
//            {
//                datarecord.custom2 = "custom2";
//            }
//            if (datarecord.custom3 == null)
//            {
//                datarecord.custom3 = "custom3";
//            }
//            if (datarecord.custom4 == null)
//            {
//                datarecord.custom4 = "custom4";
//            }
//            if (datarecord.custom5 == null)
//            {
//                datarecord.custom5 = "custom5";
//            }
//            #endregion checking null fields


//            string str = datarecord.eventdate;
//            DateTimeOffset dto = DateTimeOffset.Parse(str);
//            //Get the date object from the string. 
//            DateTime eventdate1 = dto.DateTime;
//            string eventdate2 = (dto.DateTime).ToString("dd/MM/yyyy HH:MM:ss");
            
//            string str1 = datarecord.eventdate;
//            DateTimeOffset dto1 = DateTimeOffset.Parse(str);
//            //Get the date object from the string. 
//            DateTime dateofbirth1 = dto1.DateTime;
//            string dateofbirth2 = (dto1.DateTime).ToString("dd/MM/yyyy HH:MM:ss");

//            if (test)
//            {
//                constring = ConfigurationManager.ConnectionStrings["conntest2"].ConnectionString;
//            }
//            else
//            {
//                constring = ConfigurationManager.ConnectionStrings["connlive"].ConnectionString;
//            }

//            string sql = "INSERT INTO [LeadifyStats].[dbo].[LeadifyStatisticsStrings] " +
//                        " ([eventdate],[webhooktype],[campaignid],[campaignname] " +
//                        " ,[campaigncategory],[ipaddress],[useragent], " +
//                        " [cellphoneNumber],[firstname],[lastname],[email] " +
//                        " ,[gender],[idnumber],[race],[region],[dateofbirth] " +
//                        " ,[custom1],[custom2],[custom3],[custom4],[custom5]) " +

//                        " VALUES (@eventdate,@webhooktype,@campaignid,@campaignname  " +
//                        " , @campaigncategory, @ipaddress, @useragent, @cellphoneNumber  " +
//                        " , @firstname, @lastname, @email, @gender, @idnumber, @race  " +
//                        " , @region, @dateofbirth, @custom1, @custom2, @custom3  " +
//                        " , @custom4, @custom5)";

//            await Task.Delay(3000);
//            using (SqlConnection conn = new SqlConnection(constring))
//            {
//                try
//                {
//                    conn.Open();
//                    using (SqlCommand cmd = new SqlCommand(sql, conn))
//                    {
//                        //cmd.Parameters.AddWithValue("@eventdate", Convert.ToDateTime(eventdate2));
//                        cmd.Parameters.AddWithValue("@eventdate", eventdate2);
//                        cmd.Parameters.AddWithValue("@webhooktype", datarecord.webhooktype.ToString());
//                        cmd.Parameters.AddWithValue("@campaignid", datarecord.campaignid.ToString());
//                        cmd.Parameters.AddWithValue("@campaignname", datarecord.campaignname.ToString());
//                        cmd.Parameters.AddWithValue("@campaigncategory", datarecord.campaigncategory.ToString());
//                        cmd.Parameters.AddWithValue("@ipaddress", datarecord.ipaddress.ToString());
//                        cmd.Parameters.AddWithValue("@useragent", datarecord.useragent.ToString());
//                        cmd.Parameters.AddWithValue("@cellphoneNumber", datarecord.cellphone.ToString());
//                        cmd.Parameters.AddWithValue("@firstname", datarecord.firstname.ToString());
//                        cmd.Parameters.AddWithValue("@lastname", datarecord.lastname.ToString());
//                        cmd.Parameters.AddWithValue("@email", datarecord.email.ToString());
//                        cmd.Parameters.AddWithValue("@gender", datarecord.gender.ToString());
//                        cmd.Parameters.AddWithValue("@idnumber", datarecord.idnumber.ToString());
//                        cmd.Parameters.AddWithValue("@race", datarecord.race.ToString());
//                        cmd.Parameters.AddWithValue("@region", datarecord.region.ToString());
//                        cmd.Parameters.AddWithValue("@dateofbirth", dateofbirth2);
//                        cmd.Parameters.AddWithValue("@custom1", datarecord.custom1.ToString());
//                        cmd.Parameters.AddWithValue("@custom2", datarecord.custom2.ToString());
//                        cmd.Parameters.AddWithValue("@custom3", datarecord.custom3.ToString());
//                        cmd.Parameters.AddWithValue("@custom4", datarecord.custom4.ToString());
//                        cmd.Parameters.AddWithValue("@custom5", datarecord.custom5.ToString());
//                        cmd.ExecuteNonQuery();
//                    }
//                    conn.Close();
//                }
//                catch (Exception ex)
//                {
//                    log.Info("  =  " + ex);
//                    log.Info("StatusCodeResults  =  " + "Fail");
//                    return new HttpStatusCodeResult(400, "Fail: " + ex);

//                    //var messages = Request.CreateResponse(HttpStatusCode.OK, "222");
//                    //messages.Headers.Location = new Uri(Request.RequestUri + "222");
//                    //return messages;
//                }
//            }
//            log.Info("StatusCodeResults  =  " + "Pass");
//            return new HttpStatusCodeResult(200, "Pass");
//            //var message = Request.CreateResponse(HttpStatusCode.OK, "PAss");
//            //   message.Headers.Location = new Uri(Request.RequestUri + "PAss");
//            //  return message;
//            //return new HttpStatusCodeResult(200, "Pass");
//        }
//    }
//    #endregion PushLeadStats
//}


