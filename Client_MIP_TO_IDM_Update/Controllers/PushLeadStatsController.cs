﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using MassTransit;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.Mvc;
//using System.Web.Http.Controllers;
//using System.Configuration;
//using System.Data.SqlClient;

//namespace Client_MIP_TO_IDM_Update.Controllers
//{

//    public class PushLeadStatsController : ApiController
//    {
//        #region PushLeadStats
//        [System.Web.Http.Route("PushLeadStats")]
//        public async Task<ActionResult> Post([FromUri]string emailQueueID, string campaignName, string campaignID, string list_Name,
//                    string listID, string trackingCode, string toEmailAddress, string firstName, string lastName, string cellphoneNumber,
//                    string idn, string altContact, string dob, string maritalStatus, string gender, string postal, string region,
//                    string income, string custom1_5, string dateSent, string dateOpened, string userAgent)
//        {
//            string constring;
//            bool test = false;
            
//            if (test)
//            {
//                constring = ConfigurationManager.ConnectionStrings["conntest"].ConnectionString;
//                //constring = ConfigurationManager.ConnectionStrings["connlocal"].ConnectionString;
//            }
//            else
//            {
//                constring = ConfigurationManager.ConnectionStrings["connlive"].ConnectionString;
//            }

//            string sql = "INSERT INTO [LeadifyStats].[dbo].[LeadifyInfoStats] " +
//                        "  ([emailQueueID],[campaignName],[campaignID],[list_Name] " +
//                        " ,[listID],[trackingCode],[toEmailAddress],[firstName] " +
//                        "  ,[lastName],[cellphoneNumber],[idnr],[altContact] " +
//                        " ,[dob],[maritalStatus],[gender],[postal],[region] " +
//                        "  ,[income],[custom1_5],[dateSent],[dateOpened],[userAgent]) " +

//                        " VALUES(@emailQueueID ,@campaignName,@campaignID,@list_Name  " +
//                        " ,@listID,@trackingCode,@toEmailAddress,@firstName,@lastName  " +
//                        " ,@cellphoneNumber,@idnr,@altContact,@dob,@maritalStatus,@gender  " +
//                        " ,@postal,@region,@income,@custom1_5,@dateSent,@dateOpened,@userAgent)";

//            await Task.Delay(2000);
//            try
//            {
//                //string sql = "INSERT INTO [LeadifyStats].[dbo].[test]([identifier],[name]) VALUES(@Key,@value);";
//                using (SqlConnection conn = new SqlConnection(constring))
//                {
//                    conn.Open();
//                    using (SqlCommand cmd = new SqlCommand(sql, conn))
//                    {
//                        cmd.Parameters.AddWithValue("@emailQueueID", emailQueueID.ToString());
//                        cmd.Parameters.AddWithValue("@campaignName", campaignName.ToString());
//                        cmd.Parameters.AddWithValue("@campaignID", campaignID.ToString());
//                        cmd.Parameters.AddWithValue("@list_Name", list_Name.ToString());
//                        cmd.Parameters.AddWithValue("@listID", listID.ToString());
//                        cmd.Parameters.AddWithValue("@trackingCode", trackingCode.ToString());
//                        cmd.Parameters.AddWithValue("@toEmailAddress", toEmailAddress.ToString());
//                        cmd.Parameters.AddWithValue("@firstName", firstName.ToString());
//                        cmd.Parameters.AddWithValue("@lastName", lastName.ToString());
//                        cmd.Parameters.AddWithValue("@cellphoneNumber", cellphoneNumber.ToString());
//                        cmd.Parameters.AddWithValue("@idnr", idn.ToString());
//                        cmd.Parameters.AddWithValue("@altContact", altContact.ToString());
//                        cmd.Parameters.AddWithValue("@dob", Convert.ToDateTime(dob));
//                        cmd.Parameters.AddWithValue("@maritalStatus", maritalStatus.ToString());
//                        cmd.Parameters.AddWithValue("@gender", gender.ToString());
//                        cmd.Parameters.AddWithValue("@postal", postal.ToString());
//                        cmd.Parameters.AddWithValue("@region", (region.ToString()));
//                        cmd.Parameters.AddWithValue("@income", income.ToString());
//                        cmd.Parameters.AddWithValue("@custom1_5", custom1_5.ToString());
//                        cmd.Parameters.AddWithValue("@dateSent", Convert.ToDateTime(dateSent));
//                        cmd.Parameters.AddWithValue("@dateOpened", Convert.ToDateTime(dateOpened));
//                        cmd.Parameters.AddWithValue("@userAgent", (userAgent.ToString()));
//                        cmd.ExecuteNonQuery();
//                    }
//                    conn.Close();
//                }
//                //var message = Request.CreateResponse(HttpStatusCode.OK, "OK");
//                //message.Headers.Location = new Uri(Request.RequestUri + "OK");
//                //return message;
//                return new HttpStatusCodeResult(200,"Pass");
//            }
//            catch (SqlException ex)
//            {
//                //return new EmptyResult();
//                return new HttpStatusCodeResult(400, "Fail: " + ex);
//                //return new HttpStatusCodeResult(400, "Fail" + ex);
//                /*
//                string msg = "Insert Error:";
//                msg += ex.Message;
//                var message = Request.CreateResponse(HttpStatusCode.BadRequest, "false");
//                message.Headers.Location = new Uri(Request.RequestUri + "false");
//                return message;
//                //LoggingErrorsandData.WriteErrorLog(msg + "  " + DateTime.Now);
//                */
//            }
//        }

        

//        #region Service bus
//        //public class PushLeadStatsController : ApiController
//        //{
//        //    /*
//        //    public HttpResponseMessage Post([FromUri]string emailQueueID, string campaignName, string campaignID, string list_Name, 
//        //        string listID, string trackingCode, string toEmailAddress, string firstName, string lastName,string cellphoneNumber,
//        //        string idn, string altContact, DateTime dob, string maritalStatus, string gender, string postal, string region,
//        //        string income, string custom1_5, DateTime dateSent, DateTime dateOpened, string userAgent)
//        //    */
//        //    [Route("PushLeadStats")]
//        //    public HttpResponseMessage Post([FromUri]string emailQueueID, string campaignName, string campaignID, string list_Name,
//        //        string listID, string trackingCode, string toEmailAddress, string firstName, string lastName, string cellphoneNumber,
//        //        string idn, string altContact, string dob, string maritalStatus, string gender, string postal, string region,
//        //        string income, string custom1_5,string dateSent, string dateOpened, string userAgent)
//        //    {
//        //        var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
//        //        {
//        //            var host = sbc.Host(new Uri("rabbitmq://10.0.2.197"), h =>
//        //            //var host = sbc.Host(new Uri("rabbitmq://localhost/"), h =>
//        //            {
//        //                /*
//        //                h.Username("admin");
//        //                h.Password("admin");
//        //                */
//        //                h.Username("lstats");
//        //                h.Password("lstats");
//        //            });

//        //            sbc.ReceiveEndpoint(host, "LeadifyStatisticsData", ep =>
//        //            {
//        //                ep.Handler<Client_MIP_TO_IDM_Update>(context =>
//        //                {
//        //                    return Console.Out.WriteLineAsync($"Received: {context.Message.AltContact}");
//        //                });
//        //            });
//        //        });

//        //        bus.Start();
//        //        //while  loop for testing
//        //        //while (true)
//        //        //{
//        //            bus.Publish(new Client_MIP_TO_IDM_Update
//        //            {
//        //                EmailQueueID = emailQueueID,
//        //                CampaignName = campaignName,
//        //                CampaignID = campaignID,
//        //                List_Name = list_Name,
//        //                ListID = listID,
//        //                TrackingCode = trackingCode,
//        //                ToEmailAddress = toEmailAddress,
//        //                FirstName = firstName,
//        //                LastName = lastName,
//        //                CellphoneNumber = cellphoneNumber,
//        //                ID = idn,
//        //                AltContact = altContact,
//        //                DOB = Convert.ToDateTime(dob),
//        //                MaritalStatus = maritalStatus,
//        //                Gender = gender,
//        //                Postal = postal,
//        //                Region = region,
//        //                Income = income,
//        //                Custom1_5 = custom1_5,
//        //                DateSent = Convert.ToDateTime(dateSent),
//        //                DateOpened = Convert.ToDateTime(dateOpened),
//        //                UserAgent = userAgent
//        //            });
//        //        //}
//        //        bus.Stop();
//        //        //var message = Request.CreateResponse(HttpStatusCode.Created, campaignName);
//        //        //message.Headers.Location = new Uri(Request.RequestUri + "Submited");

//        //        var message = Request.CreateResponse(HttpStatusCode.OK, "OK");
//        //        message.Headers.Location = new Uri(Request.RequestUri + "OK");
//        //        return message;
//        //    }
//        //}


//        //public class PushLeadStatsController : ApiController
//        //{



//        //    //[System.Web.Http.Route("PushLeadStats")]
//        //    //[HttpPost]
//        //    public async Task<ActionResult> mybus([FromUri]string emailQueueID, string campaignName, string campaignID, string list_Name,
//        //        string listID, string trackingCode, string toEmailAddress, string firstName, string lastName, string cellphoneNumber,
//        //        string idn, string altContact, string dob, string maritalStatus, string gender, string postal, string region,
//        //        string income, string custom1_5, string dateSent, string dateOpened, string userAgent)

//        //    {
//        //        using (var client = new HttpClient())
//        //        {
//        //            try
//        //            {
//        //                var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
//        //                {
//        //                    var host = sbc.Host(new Uri("rabbitmq://10.0.2.197"), h =>
//        //                    //var host = sbc.Host(new Uri("rabbitmq://localhost/"), h =>
//        //                    {
//        //                        /*
//        //                        h.Username("admin");
//        //                        h.Password("admin");
//        //                        */
//        //                        h.Username("lstats");
//        //                        h.Password("lstats");
//        //                    });

//        //                    sbc.ReceiveEndpoint(host, "LeadifyStatisticsData", ep =>
//        //                    {
//        //                        ep.Handler<Client_MIP_TO_IDM_Update>(context =>
//        //                        {
//        //                            return Console.Out.WriteLineAsync($"Received: {context.Message.AltContact}");
//        //                        });
//        //                    });
//        //                });

//        //                bus.Start();
//        //                //while  loop for testing
//        //                //while (true)
//        //                //{
//        //              await  bus.Publish(new Client_MIP_TO_IDM_Update
//        //                {
//        //                    EmailQueueID = emailQueueID,
//        //                    CampaignName = campaignName,
//        //                    CampaignID = campaignID,
//        //                    List_Name = list_Name,
//        //                    ListID = listID,
//        //                    TrackingCode = trackingCode,
//        //                    ToEmailAddress = toEmailAddress,
//        //                    FirstName = firstName,
//        //                    LastName = lastName,
//        //                    CellphoneNumber = cellphoneNumber,
//        //                    ID = idn,
//        //                    AltContact = altContact,
//        //                    DOB = Convert.ToDateTime(dob),
//        //                    MaritalStatus = maritalStatus,
//        //                    Gender = gender,
//        //                    Postal = postal,
//        //                    Region = region,
//        //                    Income = income,
//        //                    Custom1_5 = custom1_5,
//        //                    DateSent = Convert.ToDateTime(dateSent),
//        //                    DateOpened = Convert.ToDateTime(dateOpened),
//        //                    UserAgent = userAgent
//        //                });
//        //                //}
//        //                bus.Stop();

//        //            //return ResponseMessage(idn);

//        //            //HttpResponseMessage response =  client.GetAsync(ListID);



//        //            //var message = Request.CreateResponse(HttpStatusCode.Created, campaignName);
//        //            //message.Headers.Location = new Uri(Request.RequestUri + "Submited");

//        //            /*
//        //           var message = Request.CreateResponse(HttpStatusCode.OK, "OK");
//        //           message.Headers.Location = new Uri(Request.RequestUri + "OK");
//        //           return message;
//        //           */


//        //        }
//        //            catch (HttpRequestException)
//        //            {

//        //                return  BadRequest("Error receiving");
//        //            }
//        //         }
//        //    }


//        //    public async Task<ProcessedResponse> Post
//        //        ([FromUri]string emailQueueID, string campaignName, string campaignID, string list_Name,
//        //        string listID, string trackingCode, string toEmailAddress, string firstName, string lastName, string cellphoneNumber,
//        //        string idn, string altContact, string dob, string maritalStatus, string gender, string postal, string region,
//        //        string income, string custom1_5, string dateSent, string dateOpened, string userAgent)


//        //    {
//        //        //var client = new HttpClient();

//        //        var product = await mybus
//        //            (emailQueueID,  campaignName,  campaignID,  list_Name,
//        //         listID,  trackingCode,  toEmailAddress,  firstName,  lastName,  cellphoneNumber,
//        //         idn,  altContact,  dob,  maritalStatus,  gender,  postal,  region,
//        //         income,  custom1_5,  dateSent,  dateOpened,  userAgent);

//        //        // dbContext.Products.GetAsync(id);

//        //        if (product == null)
//        //            return NotFound();

//        //        return Ok(product);
//        //    }

//        #endregion Service bus
//    }
//    #endregion PushLeadStats

//}
