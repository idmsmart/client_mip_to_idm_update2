﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using MassTransit;

//namespace Client_MIP_TO_IDM_Update.Controllers
//{
//    public class DefaultController : ApiController
//    {
        

//        // POST: api/Default
//        public void Post([FromUri]string emailQueueID, string campaignName, string campaignID, string list_Name,
//            string listID, string trackingCode, string toEmailAddress, string firstName, string lastName, string cellphoneNumber,
//            string idn, string altContact, DateTime dob, string maritalStatus, string gender, string postal, string region,
//            string income, string custom1_5, DateTime dateSent, DateTime dateOpened, string userAgent)
//        {
//            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
//            {
//                //var host = sbc.Host(new Uri("rabbitmq://10.0.2.197"), h =>
//                var host = sbc.Host(new Uri("rabbitmq://localhost/"), h =>
//                {
//                    /*
//                    h.Username("admin");
//                    h.Password("admin");
//                    */
//                    h.Username("guest");
//                    h.Password("guest");
//                });

//                sbc.ReceiveEndpoint(host, "LeadifyStatisticsData", ep =>
//                {
//                    ep.Handler<Client_MIP_TO_IDM_Update>(context =>
//                    {
//                        return Console.Out.WriteLineAsync($"Received: {context.Message.AltContact}");
//                    });
//                });
//            });
//
//            bus.Start();

//            //while  loop for testing
//            //while (true)
//            //{
//            bus.Publish(new Client_MIP_TO_IDM_Update
//            {
//                EmailQueueID = emailQueueID,
//                CampaignName = campaignName,
//                CampaignID = campaignID,
//                List_Name = list_Name,
//                ListID = listID,
//                TrackingCode = trackingCode,
//                ToEmailAddress = toEmailAddress,
//                FirstName = firstName,
//                LastName = lastName,
//                CellphoneNumber = cellphoneNumber,
//                ID = idn,
//                AltContact = altContact,
//                DOB = dob,
//                MaritalStatus = maritalStatus,
//                Gender = gender,
//                Postal = postal,
//                Region = region,
//                Income = income,
//                Custom1_5 = custom1_5,
//                DateSent = dateSent,
//                DateOpened = dateOpened,
//                UserAgent = userAgent
//            });
//            //}

//            #region numloop
//            /*
//            for (int i = 0; i < 900; i++)
//            {
//                bus.Publish(new Client_MIP_TO_IDM_Update
//                {
//                    EmailQueueID = "1234",
//                    CampaignName = "Tesnamecap",
//                    CampaignID = "85526",
//                    List_Name = "TestListName",
//                    ListID = "0011",
//                    TrackingCode = "0025",
//                    ToEmailAddress = "abc@emai.com",
//                    FirstName = "Testtfname",
//                    LastName = "Testlastname",
//                    CellphoneNumber = "012365482",
//                    ID = "12345632",
//                    AltContact = "02552255",
//                    DOB = DateTime.Now,
//                    MaritalStatus = "M",
//                    Gender = "male",
//                    Postal = "321",
//                    Region = "Cape",
//                    Income = "552",
//                    Custom1_5 = "5",
//                    DateSent = DateTime.Now,
//                    DateOpened = DateTime.Now,
//                    UserAgent = "TestAgent"
//                });
//                Console.WriteLine("Press any key to exit");
//            }
//            //Console.ReadKey();
//            */
//            #endregion numloop

//            bus.Stop();
//            /*
//            var message = Request.CreateResponse(HttpStatusCode.OK, "OK");
//            message.Headers.Location = new Uri(Request.RequestUri + "OK");
//            return message;
//            */
//        }

//    }
//}
