﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Client_MIP_TO_IDM_Update
{
    //
    public class AllLeadStats
    {
        public string eventdate         { get; set; }
        public string campaignid        { get; set; }
        public string webhooktype       { get; set; }
        public string campaignname      { get; set; }
        public string campaigncategory  { get; set; }
        public string ipaddress         { get; set; }
        public string useragent         { get; set; }
        public string cellphone         { get; set; }
        public string firstname         { get; set; }
        public string lastname          { get; set; }
        public string email             { get; set; }
        public string gender            { get; set; }
        public string idnumber          { get; set; }
        public string race              { get; set; }
        public string dateofbirth       { get; set; }
        public string region            { get; set; }
        public string custom1           { get; set; }
        public string custom2           { get; set; }
        public string custom3           { get; set; }
        public string custom4           { get; set; }
        public string custom5           { get; set; }
    }
}