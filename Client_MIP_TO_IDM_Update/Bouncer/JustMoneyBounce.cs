﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Client_MIP_TO_IDM_Update.Bouncer
{
    public class JustMoneyBounce
    {
        public string MSISDN { get; set; }
        public string IDNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string CampaignName { get; set; }
        public string SMSCampaignID { get; set; }
        public string ListName { get; set; }
        public string DateAdded { get; set; }
        public string DateSent { get; set; }
        public string SendResult { get; set; }
        public string Lead { get; set; }
        public string LeadReceivedDate { get; set; }
        public string Optout { get; set; }
        public string OptoutReceivedDate { get; set; }
    }
}