﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadsStatsWEBAPI
{
    //adding to git
    public class Clinet_MIP_TO_IDM_Update
    {
        public string EmailQueueID { get; set; }
        public string CampaignName { get; set; }
        public string CampaignID { get; set; }
        public string List_Name { get; set; }
        public string ListID { get; set; }
        public string TrackingCode { get; set; }
        public string ToEmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CellphoneNumber { get; set; }
        public string ID { get; set; }
        public string AltContact { get; set; }
        public DateTime DOB { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string Postal { get; set; }
        public string Region { get; set; }
        public string Income { get; set; }
        public string Custom1_5 { get; set; }
        public DateTime DateSent { get; set; }
        public DateTime DateOpened { get; set; }
        public string UserAgent { get; set; }
    }
}