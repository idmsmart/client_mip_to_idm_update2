﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Client_MIP_TO_IDM_Update.MIpmodels
{
    public class clientup
    {
        public string ClientIDNumber { get; set; }
        public string ClientLeadId { get; set; }
        public string ClientUpdateStatus { get; set; }
       // public string UpdateStatus { get; set; }
        public string UniqueRefences { get; set; }
    }
    public class clientupdate
    {
        public List<clientup> L_Clients  { get; set; }
    }
}