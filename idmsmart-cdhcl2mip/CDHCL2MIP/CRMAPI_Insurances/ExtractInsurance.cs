﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data.Entity.Validation;

namespace CDHCL2MIP.CRMAPI_Insurances
{
    public class ExtractInsurance
    {

        public string APIExeutorAsync_Crm_APi(string cl_id)
        {
            string Message;

            #region APIExeutorAsync_Crm_APi
            try
            {
                var client = new RestClient(@"http://10.0.2.197:1321/api/CRM/ClientAgreement/" + cl_id);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Bearer Jeandre:0ae938aa-438f-418f-9f8b-2f893f98e0c4");
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);

                dynamic Full_mip_responce = JsonConvert.DeserializeObject(response.Content);

                if (response.StatusCode.ToString() == "GatewayTimeout")
                {
                    Message = "F";
                }
                else if (response.StatusCode.ToString() == "NotFound")
                {
                    Message = "F";
                }
                else if (response.StatusCode.ToString() != "OK")
                {
                    Message = "F";
                }
                else
                {
                    return (response.Content);
                }
                return Message;
            }
            catch (Exception e)
            {
                return "F";
            }
            #endregion

        }
    }
}
