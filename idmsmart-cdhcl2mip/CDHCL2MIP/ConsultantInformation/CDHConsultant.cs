﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDHCL2MIP.ConsultantInformation.CDHDB;

namespace CDHCL2MIP.ConsultantInformation
{
    public class CDHConsultant
    {
        public List<User> RetreiveConsultantStats(int ?user_id)
        {

            string DateRed = null;
            string timeeRed = null;
            int userid = 0;


            var crm_data = new CDHCRM2MIPCLEntities();
            try
            {
                var ClientsDatauser = (from Clintsdetails_records_users in crm_data.Users
                                       where Clintsdetails_records_users.User_UserId == user_id
                                       select Clintsdetails_records_users).ToList();
                return ClientsDatauser.ToList();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        //log.Info("Failed to retrive" + user_id + " + user_id + " + ve.ErrorMessage);
                        //log.Info("user_id" + user_id + " + user_id + " + ve.ErrorMessage);
                    }
                }
                return null;
            }
        }
    }
}
