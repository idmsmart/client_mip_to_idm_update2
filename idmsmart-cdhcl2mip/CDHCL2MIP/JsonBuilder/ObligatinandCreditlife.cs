﻿using CDHCL2MIP.IDMDXchanger.failedlead;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDHCL2MIP.IDMDXchanger;
namespace CDHCL2MIP.JsonBuilder
{
    #region ObligatinandCreditlife
    public class ObligatinandCreditlife
    {
        CRMDataDataContext crmData = new CRMDataDataContext();
        #region Obligation_creditlife
        
        public List<CreditLife> CreditLife(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, usp_SelectBudgetByOpportunityIdResult Bud,
            List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectOpportunity Oppertunity, usp_LAW_SelectCOBA_byLeadDBResult currentCOBA, usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult LeadDB)
        {
            IDM_MIP_DXchangerEntities iDM_MIP_D;
            CreditLife _CreditLife = null;
            List<CreditLife> allCreditLife = new List<CreditLife>();
            iDM_MIP_D = new IDM_MIP_DXchangerEntities();
            var crditlifecheck = iDM_MIP_D.usp_LAW_getCredit_life(LeadDB.ledb_LeadDBID).ToList();
            foreach (var crdche in crditlifecheck)
            {
                
                 if(crdche.ledb_CreditLifeInclude == "Y")
                {
                    _CreditLife = new CreditLife
                    {
                        AccountNumber = String.IsNullOrEmpty(currentCOBA.coba_accountNumber) ? "" : currentCOBA.coba_accountNumber.Trim(),
                        CreditLifeTotalCover = (double)currentCOBA.coba_outstandingbalance,
                        TotalPremium = (double)crdche.ledb_proposedcreditlife, 
                        NormalCreditLifeSold = false,
                        RetrenchmentSold = false,
                        RetrenchmentAmount = 0.0,
                        CreditLifePremium = 0.0,
                        EnhancedCreditLife = false,
                        CoverAmount = (double)currentCOBA.coba_outstandingbalance,
                    };
                    allCreditLife.Add(_CreditLife);
                }
            }
            
            //if(LeadDB.ledb_CreditLifeInclude == "Y")
       
            return allCreditLife;
        }
        public List<Obligation> Obligation(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, usp_SelectBudgetByOpportunityIdResult Bud,
            List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectOpportunity Oppertunity)
        {
            IDM_MIP_DXchangerEntities iDM_MIP_D;
            Obligation _Obligation = null;

            List<Obligation> allObligation = new List<Obligation>();

            try
            {
                List<String> doneList = new List<string>();
                foreach (usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult LeadDB in LeadDBList)
                {
                    if (LeadDB.ledb_Deleted == null)
                    {
                        if (!(String.IsNullOrWhiteSpace(LeadDB.ledb_incl_inRehab)))
                        {
                            if (!(LeadDB.ledb_incl_inRehab.ToLower() == "excluded" || LeadDB.ledb_incl_inRehab.ToLower() == "error"))
                            {
                                if (LeadDB.ledb_accnr != null && doneList.Contains(LeadDB.ledb_accnr.Trim()))
                                {
                                    //Log("Duplicate");
                                }
                                if (LeadDB.ledb_accnr != null)
                                {
                                    doneList.Add(LeadDB.ledb_accnr.Trim());
                                }

                                usp_LAW_SelectCOBA_byLeadDBResult currentCOBA;

                                currentCOBA = crmData.usp_LAW_SelectCOBA_byLeadDB(LeadDB.ledb_LeadDBID).ToList().FirstOrDefault();
                                if (currentCOBA != null)
                                {

                                    int Terms, CurrentTerms = 0;
                                    string Acctype = "";
                                    double OriginalInstallment, CurrentInstallment, OriginalBalance, CurrentBalance, InterestRate, CurrentInterestRate, MonthlyFeesExclInsurance, InsurancePremiums, Insurance = 0.0;
                                    if (Math.Round(currentCOBA.coba_orig_repayment_term.GetValueOrDefault(0), 2) > 0)
                                    {
                                        Terms = (int)Math.Round(currentCOBA.coba_orig_repayment_term.GetValueOrDefault(0), 2);
                                        //CurrentBalance = (double)Math.Round(LeadDB.ledb_.GetValueOrDefault(0), 2);
                                    }
                                    else Terms = 0;
                                    if (Math.Round(currentCOBA.coba_orig_repayment_term.GetValueOrDefault(0), 2) > 0)
                                    {
                                        CurrentTerms = (int)Math.Round(currentCOBA.coba_orig_repayment_term.GetValueOrDefault(0), 2);
                                    }
                                    else CurrentTerms = 0;
                                    if (Math.Round(currentCOBA.coba_gross_month_install.GetValueOrDefault(0), 2) > 0)
                                    {
                                        OriginalInstallment = (double)Math.Round(currentCOBA.coba_gross_month_install.GetValueOrDefault(0), 2);
                                    }
                                    else OriginalInstallment = 0.0;


                                    if (Math.Round(currentCOBA.coba_gross_month_install.GetValueOrDefault(0), 2) > 0)
                                    {
                                        CurrentInstallment = (double)Math.Round(currentCOBA.coba_gross_month_install.GetValueOrDefault(0), 2);
                                    }
                                    else CurrentInstallment = 0.0;

                                    if (Math.Round(currentCOBA.coba_oustanding_balance.GetValueOrDefault(0), 2) > 0)
                                    {
                                        OriginalBalance = (double)Math.Round(currentCOBA.coba_oustanding_balance.GetValueOrDefault(0), 2);
                                    }
                                    else OriginalBalance = 0.0;

                                    if (Math.Round(LeadDB.ledb_outstamnt.GetValueOrDefault(0), 2) > 0) //.coba_oustanding_balance.GetValueOrDefault(0), 2) > 0)
                                    {
                                        CurrentBalance = (double)Math.Round(LeadDB.ledb_outstamnt.GetValueOrDefault(0), 2);
                                    }
                                    else CurrentBalance = 0.0;

                                    if (Math.Round(currentCOBA.coba_int_rate.GetValueOrDefault(0), 2) > 0)
                                    {
                                        InterestRate = (double)Math.Round(currentCOBA.coba_int_rate.GetValueOrDefault(0), 2);
                                    }
                                    else InterestRate = 0.0;

                                    if (Math.Round(currentCOBA.coba_int_rate.GetValueOrDefault(0), 2) > 0)
                                    {
                                        CurrentInterestRate = (double)Math.Round(currentCOBA.coba_int_rate.GetValueOrDefault(0), 2);
                                    }
                                    else CurrentInterestRate = 0.0;

                                    if (Math.Round(currentCOBA.coba_monthly_fees.GetValueOrDefault(0), 2) > 0)
                                    {
                                        MonthlyFeesExclInsurance = (double)Math.Round(currentCOBA.coba_monthly_fees.GetValueOrDefault(0), 2);
                                    }
                                    else MonthlyFeesExclInsurance = 0.0;

                                    if (Math.Round(currentCOBA.coba_insurance.GetValueOrDefault(0), 2) > 0)
                                    {
                                        InsurancePremiums = (double)Math.Round(currentCOBA.coba_insurance.GetValueOrDefault(0), 2);
                                    }
                                    else InsurancePremiums = 0.0;

                                    if (Math.Round(currentCOBA.coba_insurance.GetValueOrDefault(0), 2) > 0)
                                    {
                                        Insurance = (double)Math.Round(currentCOBA.coba_insurance.GetValueOrDefault(0), 2);
                                    }
                                    else Insurance = 0.0;

                                    if (currentCOBA.coba_debt_acc_type_code != null)
                                    {
                                        if (currentCOBA.coba_debt_acc_type_code.ToLower().Trim() == "securecommvehicle" || currentCOBA.coba_debt_acc_type_code == "SCV")
                                        {
                                            Acctype = "SCV";
                                        }
                                        else if (currentCOBA.coba_debt_acc_type_code.ToLower().Trim() == "securemortgage" || currentCOBA.coba_debt_acc_type_code == "SM")
                                        {
                                            Acctype = "SM";
                                        }
                                        else if (currentCOBA.coba_debt_acc_type_code.ToLower().Trim() == "securepvtvehicle" || currentCOBA.coba_debt_acc_type_code == "SPV")
                                        {
                                            Acctype = "SPV";
                                        }
                                        else if (currentCOBA.coba_debt_acc_type_code.ToLower().Trim() == "unsecurefacility" || currentCOBA.coba_debt_acc_type_code == "UF")
                                        {
                                            Acctype = "UF";
                                        }
                                        else if (currentCOBA.coba_debt_acc_type_code.ToLower().Trim() == "UNSECUREFACILITY" || currentCOBA.coba_debt_acc_type_code == "UF")
                                        {
                                            Acctype = "UF";
                                        }
                                        else if (currentCOBA.coba_debt_acc_type_code.ToLower().Trim() == "unsecureloan" || currentCOBA.coba_debt_acc_type_code == "UT")
                                        {
                                            Acctype = "UT";
                                        }
                                        else if (currentCOBA.coba_debt_acc_type_code.ToLower().Trim() == "UNSECURELOAN" || currentCOBA.coba_debt_acc_type_code == "UT")
                                        {
                                            Acctype = "UT";
                                        }
                                        else
                                        {
                                            Acctype = "UT";
                                        }
                                    }
                                    else
                                    { Acctype = "UT"; }



                                    //string _RIDTrader = LeadDB.ledb_RID_Trader.ToString().Trim();
                                    iDM_MIP_D = new IDM_MIP_DXchangerEntities();
                                    string __CreditorDescription = null;
                                    string _AccountNumber = null;
                                    var _CreditorDescription = iDM_MIP_D.usp_LAW_getCreditodescription(LeadDB.ledb_RID_Trader).FirstOrDefault();
                                    var _HyphenCreditorLink = iDM_MIP_D.usp_getHypenCreditorLink(LeadDB.ledb_RID_Trader).FirstOrDefault();
                                    //if (_CreditorDescription != null)
                                    //{
                                    //    foreach (var crd in _CreditorDescription)
                                    //    {
                                    //        __CreditorDescription =  crd.
                                    //    }
                                    //}
                                    string rebstas = "Excluded";
                                    if (LeadDB.ledb_incl_inRehab.ToLower() == "included") rebstas = "Included";
                                    Console.WriteLine(_CreditorDescription);
                                    if(_CreditorDescription != null) __CreditorDescription = _CreditorDescription.Trim();
                                    if (LeadDB.ledb_accnr != null) _AccountNumber = LeadDB.ledb_accnr.Trim();
                                    _Obligation = new Obligation
                                    {
                                        LeadId = Convert.ToString(Lead.Lead_LeadID),
                                        AccountNumber = _AccountNumber,
                                        CreditorDescription = __CreditorDescription,
                                        PaymentProfileId = "",
                                        Terms = Terms, //(int)Math.Round(currentCOBA.coba_orig_repayment_term.GetValueOrDefault(0), 2),
                                        CurrentTerms = CurrentTerms, //(int)Math.Round(currentCOBA.coba_orig_repayment_term.GetValueOrDefault(0), 2),
                                        AccountType = Acctype,
                                        InceptionDate = String.Format("{0:yyyy-MM-dd}", Bud.budg_payPlan_startDate),// Bud.budg_payPlan_startDate,
                                        ExpiryDate = null,
                                        OriginalInstallment = OriginalInstallment,//(double)Math.Round(currentCOBA.coba_gross_month_install.GetValueOrDefault(0), 2),
                                        CurrentInstallment = CurrentInstallment, //(double)Math.Round(currentCOBA.coba_gross_month_install.GetValueOrDefault(0), 2),
                                        OriginalBalance = OriginalBalance, //(double)Math.Round(currentCOBA.coba_oustanding_balance.GetValueOrDefault(0), 2),
                                        CurrentBalance = CurrentBalance,//(double)Math.Round(currentCOBA.coba_oustanding_balance.GetValueOrDefault(0), 2),
                                        LastPayDate = null,
                                        IncludeExcludeDebtReview = rebstas,// LeadDB.ledb_incl_inRehab,
                                        RIDTrader = LeadDB.ledb_RID_Trader.ToString().Trim(),
                                        Promoted = "True",
                                        COBReceived = "YES",
                                        COBUpdated = currentCOBA.coba_cobupdated == "Y" ? "YES" : "NO",
                                        COBUpdatedDate = String.Format("{0:yyyy-MM-dd}", currentCOBA.coba_UpdatedDate),
                                        InterestRate = InterestRate, //(double)Math.Round(currentCOBA.coba_int_rate.GetValueOrDefault(0), 2),
                                        CurrentInterestRate = CurrentInterestRate,//(double)Math.Round(currentCOBA.coba_int_rate.GetValueOrDefault(0), 2),
                                        HyphenCreditorLink = String.IsNullOrEmpty(_HyphenCreditorLink) ? "" : _HyphenCreditorLink.Trim(),
                                        DateOfDefault = null,
                                        MonthlyFeesExclInsurance = MonthlyFeesExclInsurance, //(double)currentCOBA.coba_monthly_fees,
                                        InsurancePremiums = InsurancePremiums, //(double)(Math.Round(currentCOBA.coba_insurance.Value, 2)),
                                        Insurance = Insurance,//(double)(Math.Round(currentCOBA.coba_insurance.Value, 2)),
                                        Status = rebstas,//LeadDB.ledb_incl_inRehab == "Included" ? "Included" : "Excluded",
                                        CreditLife = CreditLife(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity, currentCOBA, LeadDB)
                                    };
                                    allObligation.Add(_Obligation);
                                }
                                #region DBfees

                                //else
                                //{
                                //    //this is fees
                                //    // Log("22.1 IN fees LINE 1161");

                                //    if (LeadDB == null)
                                //    {
                                //        throw new Exception("LeadDB null for --- ledb_accnr : " + LeadDB.ledb_accnr + " --- ledb_LeadDBID: " + LeadDB.ledb_LeadDBID + " --- OpportunityId: " + Oppertunity.Oppo_OpportunityId);
                                //    }

                                //    try
                                //    {
                                //        if (LeadDB.ledb_dummy.Trim().Contains("Applicationfee") || LeadDB.ledb_dummy.Trim().Contains("Administrationfee") || LeadDB.ledb_dummy.Trim().Contains("Recklesslendingfee") || LeadDB.ledb_dummy.Trim().Contains("restructurefee") || LeadDB.ledb_dummy.Trim().Contains("legalfees") || LeadDB.ledb_dummy.Trim().Contains("aftercarefee"))
                                //        {
                                //            obligation.accountcode = ConsumerImportXML.accountcode.UF;
                                //            obligation.accountnumber = "";
                                //            obligation.agreementtype = ConsumerImportXML.agreementtype.AgreementLawful;////
                                //            obligation.applyexcltoexpenses = ConsumerImportXML.yes_no.n;
                                //            obligation.clientreference = "";
                                //            obligation.cobdate = String.Format("{0:yyyy-MM-dd}", LeadDB.ledb_UpdatedDate);
                                //            obligation.cobreceivedfrom = ConsumerImportXML.cobreceivedfrom.CreditProvider; //NB Bitec
                                //            obligation.consent = ConsumerImportXML.yes_no.n;
                                //            obligation.creditlimit = 0;
                                //            obligation.creditorcode = ConsumerImportXML.creditorcode.BV; //NB Bitec
                                //            obligation.currentpaymentmethod = ConsumerImportXML.currentpaymentmethod.DebitOrder; //NB Bitec
                                //            obligation.defaultdate = null;
                                //            obligation.description = "";
                                //            obligation.currentpaymentmethodSpecified = false;
                                //            obligation.excludefrondebtreview = ConsumerImportXML.yes_no.n;
                                //            obligation.feevatamountSpecified = false;
                                //            obligation.guid = Convert.ToString(LeadDB.ledb_LeadDBID); // (String.Format("{0:dd-hh-mm-ss-tt}", DateTime.Now)).Replace("-","").Replace(" ", string.Empty); // this needs to be from DB
                                //            obligation.goodsdescription = "";
                                //            obligation.hold = ConsumerImportXML.yes_no.n;
                                //            obligation.insurance = 0;
                                //            obligation.linkedinsurancedescription = "";
                                //            obligation.lockpayment = ConsumerImportXML.yes_no.n;
                                //            obligation.monthlycharges = 0; //Passing 0 //NB Bitec
                                //            obligation.openingdate = ""; //Passing "" //NB Bitec
                                //            obligation.originalterm = 0;
                                //            obligation.outstandingcapital = 0;
                                //            obligation.outstandingterm = 0;
                                //            obligation.payment = 0;
                                //            obligation.pdapayexcluded = ConsumerImportXML.yes_no.n;
                                //            obligation.startdate = String.Format("{0:yyyy-MM-dd}", DateTime.Now);

                                //            DateTime theDate = DateTime.Now;
                                //            DateTime yearInTheFuture = theDate.AddYears(5);
                                //            obligation.expiredate = String.Format("{0:yyyy-MM-dd}", yearInTheFuture);


                                //            if (LeadDB.ledb_dummy.Trim().Contains("Applicationfee"))
                                //            {
                                //                obligation.originalintrate = 0;
                                //                obligation.originalpayment = 0;
                                //                obligation.originalbalance = Convert.ToDecimal(LeadDB.ledb_outstamnt);

                                //                string rid_trader_Applicationfee = ConfigurationManager.AppSettings["rid_trader_Applicationfee"];
                                //                obligation.rid_trader = rid_trader_Applicationfee.Trim();
                                //                obligation.clientcreditortype = ConsumerImportXML.clientcreditortype.applicationfee;
                                //                //detrectCreditortype.Add("applicationfee");
                                //                //consumerImportXML.client.obligations.Add(obligation);
                                //            }
                                //            else if (LeadDB.ledb_dummy.Trim().Contains("Administrationfee"))
                                //            {
                                //                obligation.originalintrate = 0;
                                //                obligation.originalpayment = 0;
                                //                obligation.originalbalance = Convert.ToDecimal(LeadDB.ledb_outstamnt);

                                //                string rid_trader_administrationfee = ConfigurationManager.AppSettings["rid_trader_administrationfee"];
                                //                obligation.rid_trader = rid_trader_administrationfee.Trim();
                                //                obligation.clientcreditortype = ConsumerImportXML.clientcreditortype.administrationfee;
                                //                //detrectCreditortype.Add("administrationfee");
                                //                //consumerImportXML.client.obligations.Add(obligation);
                                //            }
                                //            else if (LeadDB.ledb_dummy.Trim().Contains("Recklesslendingfee"))
                                //            {
                                //                obligation.originalintrate = 0;
                                //                obligation.originalpayment = 0;
                                //                obligation.originalbalance = Convert.ToDecimal(LeadDB.ledb_outstamnt);

                                //                string rid_trader_Recklesslendingfee = ConfigurationManager.AppSettings["rid_trader_Recklesslendingfee"];
                                //                obligation.rid_trader = rid_trader_Recklesslendingfee.Trim();
                                //                obligation.clientcreditortype = ConsumerImportXML.clientcreditortype.recklesslendingfee;
                                //                // detrectCreditortype.Add("recklesslendingfee");
                                //                //consumerImportXML.client.obligations.Add(obligation);
                                //            }

                                //            else if (LeadDB.ledb_dummy.Trim().Contains("legalfees"))
                                //            {
                                //                obligation.originalintrate = 0;
                                //                obligation.originalpayment = 0;
                                //                obligation.originalbalance = Convert.ToDecimal(LeadDB.ledb_outstamnt);

                                //                string rid_trader_legalfees = ConfigurationManager.AppSettings["rid_trader_legalfees"];
                                //                obligation.rid_trader = rid_trader_legalfees.Trim();
                                //                obligation.clientcreditortype = ConsumerImportXML.clientcreditortype.legalfees;
                                //                // detrectCreditortype.Add("legalfees");
                                //                // consumerImportXML.client.obligations.Add(obligation);
                                //            }
                                //            else if (LeadDB.ledb_dummy.Trim().Contains("restructurefee"))
                                //            {
                                //                obligation.originalintrate = 0;
                                //                obligation.originalpayment = 0;
                                //                obligation.originalbalance = Convert.ToDecimal(LeadDB.ledb_outstamnt);

                                //                string rid_trader_restructurefee = ConfigurationManager.AppSettings["rid_trader_restructurefee"];
                                //                obligation.rid_trader = rid_trader_restructurefee.Trim();
                                //                obligation.clientcreditortype = ConsumerImportXML.clientcreditortype.restructurefees;
                                //                //  detrectCreditortype.Add("restructurefees");
                                //                //  consumerImportXML.client.obligations.Add(obligation);
                                //            }
                                //            else if (LeadDB.ledb_dummy.Trim().Contains("aftercarefee"))
                                //            {
                                //                obligation.originalintrate = Convert.ToDecimal(LeadDB.ledb_outstamnt);
                                //                obligation.originalbalance = Convert.ToDecimal(LeadDB.ledb_monthinstall);
                                //                if (LeadDB.ledb_totaloutstamnt > 450)
                                //                {
                                //                    obligation.originalpayment = Convert.ToDecimal(450);
                                //                }
                                //                else
                                //                {
                                //                    obligation.originalpayment = Convert.ToDecimal(LeadDB.ledb_totaloutstamnt);
                                //                }

                                //                string rid_trader_restructurefee = ConfigurationManager.AppSettings["rid_trader_aftercarefees"];
                                //                obligation.rid_trader = rid_trader_restructurefee.Trim();
                                //                obligation.clientcreditortype = ConsumerImportXML.clientcreditortype.aftercarefees;
                                //                // detrectCreditortype.Add("aftercarefees");
                                //                // consumerImportXML.client.obligations.Add(obligation);
                                //            }

                                //            else
                                //            {
                                //                //breake;
                                //                // Log("Nothing was found in dummy");
                                //            }
                                //        }
                                //    }
                                //    catch
                                //    {
                                //        //Log("coba_debt_acc_type_code errors occured" + current.OpportunityId);
                                //    }
                                //} 
                                #endregion
                            }
                            else
                            {
                                // Log("if (!(LeadDB.ledb_incl_inRehab.ToLower() == excluded || LeadDB.ledb_incl_inRehab.ToLower() == error))");
                            }
                        }
                        else
                        {
                            //Log("please set value include exclude on the obligation : Account number : " + LeadDB.ledb_accnr);
                            //throw new Exception("please set value include exlude on the obligation : Account number : " + LeadDB.ledb_accnr);
                        }
                    }
                    else
                    {
                        // Log("Deleted oblication");
                    }
                }
            }
            catch (Exception ex)
            {
                BitechConsumerExportTEntities1 entities1_mip = new BitechConsumerExportTEntities1();
                entities1_mip.Insertfailedleadcdhtomip(Lead.Lead_LeadID);
                Console.WriteLine(ex.Message.ToString());
            }

            return allObligation;

        }
        #endregion
    } 
    #endregion
}
