﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDHCL2MIP.IDMBi01Insurance;
using CDHCL2MIP.CRMAPI_Insurances;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using CDHCL2MIP.ConsultantInformation;
using log4net;

namespace CDHCL2MIP.JsonBuilder
{

    public class InsuracesGround
    {
        ILog log = LogManager.GetLogger(typeof(InsuracesGround));
        #region Insurance

        #region Life

        #region LifeBeneficiary
        public List<LifeBeneficiary> LifeBeneficiary(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, usp_SelectBudgetByOpportunityIdResult Bud,
        List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectOpportunity Oppertunity, Guid Client_id, string polinumber, dynamic Full_mip_responce)
        {

            LifeBeneficiary _LifeBeneficiary = null;
            List<LifeBeneficiary> allLifeBeneficiary = new List<LifeBeneficiary>();

            try
            {

                dynamic LifeCover_ = JsonConvert.DeserializeObject(Full_mip_responce);

                dynamic Jtoken = LifeCover_.LifeCover.Beneficiaries;

                foreach (var crediitls in Jtoken)
                {

                    dynamic Jtokens = crediitls;
                    double pcs = 0.0;
                    if (Jtokens.SharePercentage == null)
                    {
                        Jtokens.SharePercentage = pcs;
                    }
                    else pcs = Jtokens.SharePercentage;


                    _LifeBeneficiary = new LifeBeneficiary
                    {
                        PolicyNumber = Jtokens.PolicyNumber,
                        FirstName = Regex.Replace((string)Jtokens.FirstName, @"\s+", string.Empty),
                        LastName = Jtokens.LastName,
                        BirthDate = String.Format("{0:yyyy-MM-dd}", (string)Jtokens.DateOfBirth),
                        Gender = Jtokens.Gender,
                        MainClientIDNumber = (string)Jtokens.MainClientIDNumber,
                        Relationship = Jtokens.Relationship,
                        SharePercentage = pcs,
                        CreatedBy = GetUserCreato((int)Jtokens.CreatedBy),
                        IsMainBeneficiary = Jtokens.IsMainBeneficiary
                    };
                }
                allLifeBeneficiary.Add(_LifeBeneficiary);

                return allLifeBeneficiary;
            }
            catch (Exception ex)
            {
                log.Info("crm errors " + ex.Message.ToString());
                return null;
            }
        }

        #endregion

        #region LifeCover
        public List<LifeCover> LifeCover(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, usp_SelectBudgetByOpportunityIdResult Bud,
           List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectOpportunity Oppertunity, Guid Client_id, dynamic Full_mip_responce)
        {

            LifeCover _LifeCover = null;
            List<LifeCover> allLifeCover = new List<LifeCover>();

            try
            {

                dynamic lifecover_ = JsonConvert.DeserializeObject(Full_mip_responce);

                dynamic Jtoken = lifecover_.LifeCover;

                double _IncomeReplacementAmount = 0.0;
                if (Jtoken.IncomeReplacementAmount == null)
                {
                    Jtoken.IncomeReplacementAmount = _IncomeReplacementAmount;
                }
                else _IncomeReplacementAmount = Jtoken.IncomeReplacementAmount;




                double _LifeCoverAmount = 0.0;
                if (Jtoken.LifeCoverAmount == null)
                {
                    Jtoken._LifeCoverAmount = _LifeCoverAmount;
                }
                else _LifeCoverAmount = Jtoken.LifeCoverAmount;

                double _Premium = 0.0;
                if (Jtoken.LifeCoverAmount == null)
                {
                    Jtoken.Premium = _Premium;
                }
                else _Premium = Jtoken.Premium;


                double _PermanentDisabilityAmount = 0.0;
                if (Jtoken.PermanentDisabilityAmount == null)
                {
                    Jtoken.PermanentDisabilityAmount = _PermanentDisabilityAmount;
                }
                else _PermanentDisabilityAmount = Jtoken.PermanentDisabilityAmount;


                double _DreadDiseaseAmount = 0.0;
                if (Jtoken.DreadDiseaseAmount == null)
                {
                    Jtoken.DreadDiseaseAmount = _DreadDiseaseAmount;
                }
                else _DreadDiseaseAmount = Jtoken.DreadDiseaseAmount;

                _LifeCover = new LifeCover
                {
                    LeadId = Convert.ToString(Lead.Lead_LeadID),
                    PolicyNumber = "",
                    CoverAmount = _LifeCoverAmount,
                    Premium = _Premium,
                    PermanentDisabilityAmount = _PermanentDisabilityAmount,
                    DreadDiseaseAmount = _DreadDiseaseAmount,
                    IncomeReplacementAmount = _IncomeReplacementAmount,
                    CreatedBy = GetUserCreato((int)Jtoken.CreatedBy),
                    LifeBeneficiary = LifeBeneficiary(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity, Client_id, "", Full_mip_responce)
                };

                //}
                allLifeCover.Add(_LifeCover);

                return allLifeCover;
            }
            catch (Exception ex)
            {
                log.Info("crm errors " + ex.Message.ToString());
                return null;
            }
        }

        #endregion

        #endregion

        #region FuneralBeneficiary
        public List<FuneralBeneficiary> FuneralBeneficiary(dynamic Full_mip_responce)
        {
            FuneralBeneficiary _FuneralBeneficiary = null;
            List<FuneralBeneficiary> allFuneralBeneficiary = new List<FuneralBeneficiary>();

            try
            {

                dynamic FuneralCover_ = JsonConvert.DeserializeObject(Full_mip_responce);

                dynamic Jtoken = FuneralCover_.FuneralCover.Beneficiaries;

                foreach (var crediitls in Jtoken)
                {
                    dynamic Jtokens = crediitls;
                    double pcs = 0.0;
                    if (Jtokens.SharePercentage == null)
                    {
                        Jtokens.SharePercentage = pcs;
                    }
                    else pcs = Jtokens.SharePercentage;

                    _FuneralBeneficiary = new FuneralBeneficiary
                    {
                        PolicyNumber = Jtokens.PolicyNumber,
                        FirstName = Regex.Replace((string)Jtokens.FirstName, @"\s+", string.Empty),
                        LastName = Jtokens.LastName,
                        BirthDate = String.Format("{0:yyyy-MM-dd}", Jtokens.DateOfBirth),
                        Gender = Jtokens.Gender,
                        MainClientIDNumber = Jtokens.MainClientIDNumber,
                        Relationship = Jtokens.Relationship,
                        SharePercentage = pcs,
                        CreatedBy = GetUserCreato(Jtokens.CreatedBy),
                        IsMainBeneficiary = Jtokens.IsMainBeneficiary
                    };
                    allFuneralBeneficiary.Add(_FuneralBeneficiary);
                }

                return allFuneralBeneficiary;
            }
            catch (Exception ex)
            {
                log.Info("crm errors " + ex.Message.ToString());
                return null;
            }
        }
        #endregion

        #region FuneralCover

        public List<FuneralCover> FuneralCover(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, usp_SelectBudgetByOpportunityIdResult Bud,
            List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectOpportunity Oppertunity, Guid Client_id, dynamic Full_mip_responce)
        {
            FuneralCover _FuneralCover = null;
            List<FuneralCover> allFuneralCover = new List<FuneralCover>();

            try
            {
                string _policy = null;
                dynamic FuneralCover_ = JsonConvert.DeserializeObject(Full_mip_responce);
                foreach (var crediitl in FuneralCover_.FuneralCover)
                {
                    dynamic Jtokenff = FuneralCover_.FuneralCover.Beneficiaries;

                    foreach (var crediitls in Jtokenff)
                    {
                        dynamic Jtokens = crediitls;
                        _policy = (string)Jtokens.PolicyNumber;
                        break;
                    }
                }

                dynamic Jtoken = FuneralCover_.FuneralCover;
                _FuneralCover = new FuneralCover
                {
                    LeadId = Convert.ToString(Lead.Lead_LeadID),
                    PolicyNumber = _policy,
                    CoverAmount = Jtoken.CoverAmount,
                    Premium = Jtoken.Premium,
                    FuneralDate = Jtoken.FuneralDate,
                    FuneralFirstPayDate = Jtoken.FuneralFirstPayDate,
                    PeopleCovered = Jtoken.PeopleCovered,
                    AgeOfOldest = Jtoken.AgeOfOldest,
                    FuneralBeneficiary = FuneralBeneficiary(Full_mip_responce)
                };

                allFuneralCover.Add(_FuneralCover);

                return allFuneralCover;
            }
            catch (Exception ex)
            {
                log.Info("crm errors " + ex.Message.ToString());
                return null;
            }
        }


        #endregion

        #region CreditLifeIns
        public List<CreditLifeIn> CreditLifeIns(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, usp_SelectBudgetByOpportunityIdResult Bud,
         List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectOpportunity Oppertunity, Guid Client_id, dynamic Full_mip_responce)
        {
            CreditLifeIn _CreditLifeIn = null;
            List<CreditLifeIn> allCreditLifeIn = new List<CreditLifeIn>();

            try
            {
                dynamic lifecovers = JsonConvert.DeserializeObject(Full_mip_responce);
                foreach (var crediitl in lifecovers.LifeCover)
                {
                    dynamic Jtoken = lifecovers.LifeCover.Beneficiaries;
                    _CreditLifeIn = new CreditLifeIn
                    {
                        LeadId = Convert.ToString(Lead.Lead_LeadID),
                        PolicyNumber = "",
                        CoverAmount = Jtoken.NormalCreditLifeSold,
                        RehabCovered = 0.0,
                        CreditLifePremium = 0.0,
                        RetrenchmentAmount = Jtoken.RetrenchmentSold,
                        TotalPremium = 0.0,
                        EffectiveDate = Jtoken.EmailSentDate,
                    };

                    allCreditLifeIn.Add(_CreditLifeIn);

                }

                return allCreditLifeIn;

            }
            catch (Exception ex)
            {
                log.Info("crm errors " + ex.Message.ToString());
                return null;
            }
        }

        #endregion

        #region EnhancedCreditLife
        public List<EnhancedCreditLife> EnhancedCreditLife(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, usp_SelectBudgetByOpportunityIdResult Bud,
    List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectOpportunity Oppertunity)
        {
            EnhancedCreditLife _EnhancedCreditLife = null;
            List<EnhancedCreditLife> allEnhancedCreditLife = new List<EnhancedCreditLife>();

            try
            {
                _EnhancedCreditLife = new EnhancedCreditLife
                {
                    LeadId = Convert.ToString(Lead.Lead_LeadID),
                    PolicyNumber = "",
                    CoverAmount = 0.0,
                    RehabCovered = 0.0,
                    CreditLifePremium = 0.0,
                    RetrenchmentAmount = 0.0,
                    TotalPremium = 0.0,
                    EffectiveDate = null
                };
                allEnhancedCreditLife.Add(_EnhancedCreditLife);


                return allEnhancedCreditLife;
            }
            catch (Exception ex)
            {
                //log.Info("Erros occured during _session_details thread : " + ex.Message.ToString());
                return null;
            }
        }

        #endregion

        #region InsurancefromDB
        public List<Insurance> InsurancefromDB(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, usp_SelectBudgetByOpportunityIdResult Bud,
         List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectOpportunity Oppertunity, usp_LAW_SelectCompanyResult Company)
        {
            try
            {
                Guid Client_id = Guid.Empty;
                Insurance _Insurance = null;
                List<Insurance> allInsurance = new List<Insurance>();

                ExtractInsurance _insurance = new ExtractInsurance();

                string Full_mip_responce = _insurance.APIExeutorAsync_Crm_APi(Company.comp_idnr.Trim());

                bool credlf = true;
                dynamic crediitl_ = JsonConvert.DeserializeObject(Full_mip_responce);
                foreach (var crediitl in crediitl_.CreditLife)
                {
                    dynamic Jtoken = crediitl_.CreditLife;
                    if ((string)Jtoken.Sold == "False") credlf = false;
                }


                if (credlf == true)
                {
                    _Insurance = new Insurance
                    {
                        LeadId = Convert.ToString(Lead.Lead_LeadID),
                        CreditLifeIns = CreditLifeIns(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity, Client_id, Full_mip_responce),
                        FuneralCover = FuneralCover(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity, Client_id, Full_mip_responce),
                        LifeCover = LifeCover(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity, Client_id, Full_mip_responce),
                    };
                    allInsurance.Add(_Insurance);
                    return allInsurance;
                }
                else
                {
                    _Insurance = new Insurance
                    {
                        LeadId = Convert.ToString(Lead.Lead_LeadID),
                        FuneralCover = FuneralCover(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity, Client_id, Full_mip_responce),
                        LifeCover = LifeCover(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity, Client_id, Full_mip_responce),
                    };
                    allInsurance.Add(_Insurance);
                    return allInsurance;
                }
            }
            catch (Exception ex)
            {
                log.Info("crm errors " + ex.Message.ToString());
                return null;
            }
        }

        #endregion


        #endregion


        #region GetUserCreato

        public string GetUserCreato(int userid)
        {
            CDHConsultant cDHCL = new CDHConsultant();
            var consult_details = cDHCL.RetreiveConsultantStats(userid);
            string _FAConsultantName = "";
            string _FCConsultantName = "";
            foreach (var detailsdata in consult_details)
            {
                _FAConsultantName = String.IsNullOrEmpty(detailsdata.User_FirstName) ? "" : detailsdata.User_FirstName.Trim();
                _FCConsultantName = String.IsNullOrEmpty(detailsdata.User_LastName) ? "" : detailsdata.User_LastName.Trim();

            }
            return _FAConsultantName + " " + _FCConsultantName;

        }
        #endregion
    }
}
