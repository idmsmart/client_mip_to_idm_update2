﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDHCL2MIP.ConsultantInformation.CDHDB;
using CDHCL2MIP.ConsultantInformation;
using CDHCL2MIP.IDMDXchanger;
using log4net;

namespace CDHCL2MIP.JsonBuilder
{
    public class cdhclient
    {
        CRMDataDataContext crmData = new CRMDataDataContext();
        FullBudget budget = new FullBudget();
        ObligatinandCreditlife obscred = new ObligatinandCreditlife();
        InsuracesGround insuraces = new InsuracesGround();
        MappingIdmmip idmmip = new MappingIdmmip();
        ILog log = LogManager.GetLogger(typeof(cdhclient));
        public Root Root(usp_LAW_SelectOpportunity Oppertunity, usp_LAW_SelectCompanyResult Company, usp_AddressByAddressIdResult Address, usp_BankByCompanyIdResult Bank, usp_LAW_SelectPersonResult Person, usp_getCellNumberResult Cell, usp_LAW_SelectLeadResult Lead,
            usp_SelectBudgetByOpportunityIdResult Bud, List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp)
        {
            JsonSerializerSettings objJsonSettings = null;
            objJsonSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Include };
            Root _root = null;
            _root = new Root
            {
                dsFBAImportIDMData = DsFBAImportIDMData(Oppertunity, Company, Address, Bank, Person, Cell, Lead, Bud, LeadDBList, LeadDBExp)
            };
            string fulljason = null;
            fulljason = JsonConvert.SerializeObject(_root, Formatting.Indented, objJsonSettings);
            return _root;
        }

        public DsFBAImportIDMData DsFBAImportIDMData(usp_LAW_SelectOpportunity Oppertunity, usp_LAW_SelectCompanyResult Company, usp_AddressByAddressIdResult Address, usp_BankByCompanyIdResult Bank, usp_LAW_SelectPersonResult Person, usp_getCellNumberResult Cell, usp_LAW_SelectLeadResult Lead, usp_SelectBudgetByOpportunityIdResult Bud,
            List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp)
        {

            DsFBAImportIDMData _DsFBAImportIDMData = null;

            _DsFBAImportIDMData = new DsFBAImportIDMData
            {
                ttLeadDetails = TtLeadDetail(Oppertunity, Company, Address, Bank, Person, Cell, Lead, Bud, LeadDBList, LeadDBExp)
            };
            return _DsFBAImportIDMData;
        }

        #region TtLeadDetail
        public List<TtLeadDetail> TtLeadDetail(usp_LAW_SelectOpportunity Oppertunity, usp_LAW_SelectCompanyResult Company, usp_AddressByAddressIdResult Address, usp_BankByCompanyIdResult Bank,
                usp_LAW_SelectPersonResult Person, usp_getCellNumberResult Cell, usp_LAW_SelectLeadResult Lead, usp_SelectBudgetByOpportunityIdResult Bud,
                 List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp)
        {
   
            try
            {
                //CRMDataDataContext crmData = new CRMDataDataContext();
                TtLeadDetail _ttLeadDetails = null;
                List<TtLeadDetail> allttLeadDetails = new List<TtLeadDetail>();
                string _FAConsultantName = null;
                string _FCConsultantName = null;
                string _FCConsultantEmail = null;
                string _FCConsultantPhone = null;
                string _FCConsultantTeam = null;
                NotesandStatus notesandStatus = new NotesandStatus();

                CDHConsultant cDHCL = new CDHConsultant();
                var consult_details = cDHCL.RetreiveConsultantStats(Company.Comp_PrimaryUserId);

                foreach (var detailsdata in consult_details)
                {
                    _FAConsultantName = String.IsNullOrEmpty(detailsdata.User_FirstName) ? "" : detailsdata.User_FirstName.Trim();
                    _FCConsultantName = String.IsNullOrEmpty(detailsdata.User_LastName) ? "" : detailsdata.User_LastName.Trim();
                    _FCConsultantEmail = String.IsNullOrEmpty(detailsdata.User_EmailAddress) ? "" : detailsdata.User_EmailAddress.Trim();
                    _FCConsultantPhone = String.IsNullOrEmpty(detailsdata.User_Extension) ? "" : detailsdata.User_Extension.Trim();
                    _FCConsultantTeam = String.IsNullOrEmpty(detailsdata.User_Department) ? "" : detailsdata.User_Department.Trim();
                }



                string _ProvisionalProposalSent = "";//= String.IsNullOrEmpty(Bank.bank_accnumber) ? "" : Bank.bank_accnumber.Trim(),
                string _ProvisionalProposalDateSent = "";//= String.Format("{0:yyyy-MM-dd}", dob),
                string _FinalProposalSent = "";//=  String.IsNullOrEmpty(Bank.bank_accnumber) ? "" : Bank.bank_accnumber.Trim(),
                string _FinalProposalDateSent = "";//= String.Format("{0:yyyy-MM-dd}", dob),

                IDM_MIP_DXchangerEntities provipop = new IDM_MIP_DXchangerEntities();
                var provpop = provipop.usp_LAW_getprovisionaldates(Oppertunity.Oppo_LeadID).ToList();
                if (provpop != null)
                {
                    foreach (var single in provpop)
                    {
                        _ProvisionalProposalSent = String.IsNullOrEmpty(single.ledb_provpropstatus) ? "" : single.ledb_provpropstatus.Trim();
                        _ProvisionalProposalDateSent = String.IsNullOrWhiteSpace(String.Format("{0:yyyy-MM-dd}", single.ledb_provproposalsent)) ? "" : String.Format("{0:yyyy-MM-dd}", single.ledb_provproposalsent);
                        _FinalProposalSent = String.IsNullOrEmpty(single.ledb_finalproposalstatus) ? "" : single.ledb_finalproposalstatus.Trim();
                        _FinalProposalDateSent = String.IsNullOrWhiteSpace(String.Format("{0:yyyy-MM-dd}", single.ledb_finalpropsalsent)) ? "" : String.Format("{0:yyyy-MM-dd}", single.ledb_finalpropsalsent);
                    }
                }


                string firstnames = Getfirsname(Company);
                //string _GetLastName = GetLastName(Company);
                DateTime dob = GetDoB(Company);
                string comp_race = Getrace(Company);
                string gednder = Getgender(Company);
                string maritalstatus = GetMaritalstat(Company);
                string cellnum = Getcenm(Company);
                sbyte comp_dependants = Getnubdeb(Company);
                string add1 = Getadreedd(Address);
                string add2 = Getadreedd2(Address);
                string add3 = Getadreedd3(Address);
                string add4 = Getadreedd4(Address);
                string betbanktype = Getbanktype(Bank);
                string Stageoppo = OppoStage(Company);
                string get_leg_status = ClientLegalstatus(Company);

                //Person =  Spouse chunking
                string SpouseFirstName = null;// String.IsNullOrEmpty(Person.Pers_FirstName) ? "" : Person.Pers_FirstName.Trim();
                string SpouseLastName = null;//  String.IsNullOrEmpty(Person.Pers_LastName) ? "" : Person.Pers_LastName.Trim();
                string SpouseIDNumber = null;//  String.IsNullOrEmpty(Person.Pers_MiddleName) ? "" : Person.Pers_MiddleName.Trim();
                string SpouseCellnumber = null;//  String.IsNullOrEmpty(Person.Pers_MiddleName) ? "" : Person.Pers_MiddleName.Trim();
                if (Person != null)
                {
                    SpouseFirstName = String.IsNullOrEmpty(Person.Pers_FirstName) ? "" : Person.Pers_FirstName.Trim();
                    SpouseLastName = String.IsNullOrWhiteSpace(Person.Pers_LastName) ? "" : Person.Pers_LastName.Trim();
                    SpouseIDNumber = String.IsNullOrWhiteSpace(Person.Pers_MiddleName) ? "" : Person.Pers_MiddleName.Trim();
                    //SpouseCellnumber = String.IsNullOrWhiteSpace(Person.Pers_) ? "" : Person.Pers_MiddleName.Trim();
                }
                string collctype = "";// GetCollecttype(Bud);

                string id_type = "er_AcPerIDTypeIdd";
                if (Company.comp_idnr != null)
                {
                    int cnt = Company.comp_idnr.Trim().Length;
                    if (cnt == 13)
                    {
                        id_type = "er_AcPerIDTypeIdd";
                    }
                    else
                    {
                        id_type = "er_AcPerIDTypePas";
                    }
                }
                string _lead_companygender = provipop.usplead_companygenderLead(Lead.Lead_LeadID).FirstOrDefault();
                //LeadDB.ledb_accnr;
                //er_AcPerIDTypeIdd = ID
                //er_AcPerIDTypePas = Passport
                _ttLeadDetails = new TtLeadDetail
                {
                    LeadID = Convert.ToString(Lead.Lead_LeadID),
                    CRMLeadID = Convert.ToString(Lead.Lead_LeadID),
                    Title = Lead.lead_companytitle,
                    FirstName = firstnames,
                    LastName = Company.Comp_Name.Trim().Substring(Company.Comp_Name.Trim().IndexOf(' ')),
                    BirthDate = String.Format("{0:yyyy-MM-dd}", dob),
                    IDType = id_type,
                    IDNumber = String.IsNullOrEmpty(Company.comp_idnr) ? "" : Company.comp_idnr.Trim(),
                    Race = comp_race,
                    Gender = _lead_companygender,
                    MaritalStatus = maritalstatus,
                    Email = Oppertunity.oppo_scenario,
                    CellphoneNumber = cellnum,
                    TelephoneNumber = "",
                    NumberOfDependents = comp_dependants,
                    ResidentialAddress1 = add1,
                    ResidentialAddress2 = add2,
                    ResidentialAddress3 = add3,
                    ResidentialCity = add4,
                    ResidentialProvince = "",
                    ResidentialZipCode = "",
                    PostalAddress = "",
                    PostalCity = "",
                    PostalProvince = "",
                    PostalZipCode = "",
                    Occupation = String.IsNullOrEmpty(Company.comp_occupation) ? "" : Company.comp_occupation.Trim(),
                    JobDescription = "",
                    PayDate = 0,
                    EmployerName = String.IsNullOrEmpty(Company.comp_employer) ? "" : Company.comp_employer.Trim(),
                    EmployerAddress = "",
                    EmployerZipCode = "",
                    EmployerPhone = "",
                    EmployerRisk = "",
                    IncomeBracket = "",
                    SpouseFirstName = SpouseFirstName,
                    SpouseLastName = SpouseLastName,
                    SpouseIDNumber = SpouseIDNumber,
                    SpouseEmail = "",
                    SpouseCellphoneNumber = "",
                    SpouseJobDescription = "",
                    SpousePayDate = 0,
                    SpouseEmployerName = "",
                    SpouseEmployerAddress = "",
                    SpouseEmployerZipCode = "",
                    SpouseEmployerPhone = "",
                    SpouseRace = "",
                    SpouseBirthDate = null,
                    SpouseCreditScore = "",
                    CreatedDate = Lead.Lead_CreatedDate?.ToString("yyyy-MM-dd"), //jt
                    SearchKeywords = "",//
                    DeviceWhenQuery = "",//
                    ReasonForEnquiry = "",//
                    ApplicationType = "",
                    Form17_1Date = String.Format("{0:yyyy-MM-dd}", Oppertunity.oppo_Date17_1Sent), //
                    Form17_2Date = String.Format("{0:yyyy-MM-dd}", Oppertunity.oppo_17_2Sent), //
                    Form17_2Type = "", //
                    RejectReason = "",
                    Form17_WDate = null, //
                    Withdrawal_YN = "", //
                    BankName = String.IsNullOrEmpty(Bank.bank_Name) ? "" : Bank.bank_Name.Trim(),
                    BankBranchCode = Bank.bank_branchcode,
                    AccountType = betbanktype,
                    BankAccountNumber = String.IsNullOrEmpty(Bank.bank_accnumber) ? "" : Bank.bank_accnumber.Trim(),
                    CollectionType = collctype,
                    BankAVSResult = "",
                    PaymentScheduleSent = Oppertunity.oppo_paymentSchedule_sent,//
                    PaymentScheduleDate = Oppertunity.oppo_17_2Sent?.ToString("yyyy-MM-dd"),
                    UpdatedDate = null,
                    DebitOrder1Date = String.Format("{0:yyyy-MM-dd}", Bud.budg_tempDODate1),
                    DebitOrder2Date = String.Format("{0:yyyy-MM-dd}", Bud.budg_tempDODate2),
                    CreditScore = "",///
                    DCName = Lead.lead_DCName,
                    DCNumber = Oppertunity.oppo_DCNumber, //"NCRDC1801",  //to get dc

                    Stage = Stageoppo,
                    Status = String.IsNullOrEmpty(Company.comp_clientStatus) ? "" : Company.comp_clientStatus.Trim(),

                    Source = String.IsNullOrEmpty(Lead.Lead_Source) ? "" : Lead.Lead_Source.Trim(),

                    WebSource = String.IsNullOrEmpty(Lead.lead_websource) ? "" : Lead.lead_websource.Trim(),

                    Campaign = Lead.lead_campaign,

                    FAConsultantName = "",
                    FCConsultantName = _FAConsultantName + " " + _FCConsultantName,
                    FCConsultantEmail = _FCConsultantEmail,
                    FCConsultantPhone = _FCConsultantPhone,
                    FCConsultantTeam = _FCConsultantTeam,
                    ClientAcceptance =  "",

                    //this needs be updated
                    LegalStatus = get_leg_status,
                    IndustryRisk = "",


                    FirstPayment = 0.0,
                    QuotedRehab = Double.Parse((LeadDBExp.lede_rehab?.ToString() ?? "0")),
                    FinalAgreedAmount = 0.0,
                    DCRSScenario = "",
                    RestructureFee = Double.Parse((Bud.budg_restructureFeeMC?.ToString() ?? "0")),
                    ApplicationFee = 345.0,
                    LegalFee = Double.Parse((Bud.budg_legal_fee?.ToString() ?? "0")),
                    RecklessLendingFee = Double.Parse((Bud.budg_adminFeeAmount1?.ToString() ?? "0")),
                    AftercareFee = 0.0,
                    WPSentDate = Lead.lead_date16_1sent?.ToString("yyyy-MM-dd"),
                    WPSignedDate = Lead.lead_date16_1received?.ToString("yyyy-MM-dd"),
                    WPReceivedDate = Lead.lead_date16_1received?.ToString("yyyy-MM-dd"),

                    ProvisionalProposalSent = string.IsNullOrEmpty(_ProvisionalProposalDateSent) ? "No" : "Yes",// String.IsNullOrEmpty(Bank.bank_accnumber) ? "" : Bank.bank_accnumber.Trim(),
                    ProvisionalProposalDateSent = _ProvisionalProposalDateSent, ////String.Format("{0:yyyy-MM-dd}", dob),
                    FinalProposalSent = string.IsNullOrEmpty(_FinalProposalDateSent) ? "No" : "Yes",// String.IsNullOrEmpty(Bank.bank_accnumber) ? "" : Bank.bank_accnumber.Trim(),
                    FinalProposalDateSent = _FinalProposalDateSent,//String.Format("{0:yyyy-MM-dd}", dob),
                    PreviousDebtCounselling = "false",

                    Notes = notesandStatus.GetNotes(Lead.Lead_LeadID),
                    Stages = notesandStatus.GetStages(Lead.Lead_LeadID),
                    Budget = budget.Budget(Lead, Bud, LeadDBExp, LeadDBList),
                    Obligation = obscred.Obligation(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity),
                    Insurance = insuraces.InsurancefromDB(Lead, LeadDBExp, Bud, LeadDBList, Oppertunity, Company)
                };
                allttLeadDetails.Add(_ttLeadDetails);
                return allttLeadDetails;

            }
            catch (Exception ex)
            {
                Console.WriteLine();
                log.Info(ex.Message.ToString());
                return null;
            }

        }
        #region InnerMethods
        private static string GetCollecttype(usp_SelectBudgetByOpportunityIdResult Bud)
        {
            string collctype;
            if (Bud.budg_paymentMethodM_R.ToLower().Contains("pda"))
            {
                collctype = "pda";
            }
            else if (Bud.budg_paymentMethodM_R.ToLower().Contains("manual"))
            {
                collctype = "manual";
            }
            else if (Bud.budg_paymentMethodM_R.ToLower().Contains("debit"))
            {
                collctype = "debitorder";
            }
            else
            {
                collctype = "debitorder";
            }

            return collctype;
        }

        private static string Getbanktype(usp_BankByCompanyIdResult Bank)
        {
            string bankaccounttype = null;

            if (Bank.bank_type.ToLower().Trim() == "savings")
            {
                bankaccounttype = "SAV";
            }
            else if (Bank.bank_type.ToLower().Trim() == "cheque")
            {
                bankaccounttype = "CHQ";
            }
            else if (Bank.bank_type.ToLower().Trim() == "subshare")
            {
                bankaccounttype = "subshare";
            }
            else if (Bank.bank_type.ToLower().Trim() == "transmission")
            {
                bankaccounttype = "transmission";
            }
            else
            {
                bankaccounttype = "subshare";
            }
            return bankaccounttype;
        }

        #region old
        //private static string CompStage(usp_LAW_SelectCompanyResult companyss)
        //{
        //    MappingIdmmip idmmip = new MappingIdmmip();
        //    string OppoStages = null;

        //    if (companyss.Comp_Status == "NCTCOG")
        //    {
        //        OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("NCT");
        //    }
        //    else if (Oppertunity.Oppo_Stage.Trim() == "SavingSheet")
        //    {
        //        OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("Negotiations Finalised");
        //    }
        //    else if (Oppertunity.Oppo_Stage.Trim() == "DCRS")
        //    {
        //        OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("Assigned to Negotiations");
        //    }
        //    else if (Oppertunity.Oppo_Stage.Trim() == "NCTComplete")
        //    {
        //        OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("CPs Notified NCT Application");
        //    }
        //    else if (Oppertunity.Oppo_Stage.Trim() == "NCTComplete")
        //    {
        //        OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("CPs Notified NCT Application");
        //    }
        //    else if (Oppertunity.Oppo_Stage.Trim() == "NCTComplete")
        //    {
        //        OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("CPs Notified NCT Application");
        //    }
        //    else
        //    {
        //        OppoStages = Oppertunity.Oppo_Stage.Trim();
        //    }
        //    return OppoStages;
        //} 
        #endregion

        private static string OppoStage(usp_LAW_SelectCompanyResult Company)
        {
            MappingIdmmip idmmip = new MappingIdmmip();
            IDM_MIP_DXchangerEntities entitystage = new IDM_MIP_DXchangerEntities();
            var stage = entitystage.usp_LAW_SelectCompanyForImport2MIP(Company.comp_idnr);
            string tempstage = null;
            foreach (var singlesg in stage)
            {
                tempstage = singlesg.comp_stage;
            }
            if (tempstage != null)
            {
                return idmmip.Maaping_MIP_Field_to_IDM_Field(tempstage.Trim());
            }
            else
            {
                return "Accepted";
            }
        }


        private static string ClientStage(usp_LAW_SelectCompanyResult Company)
        {

            string legstts = null;
            using (var comlegalstage = new CDHCRM2MIPCLEntities())
            {

                var singleuniqueref = comlegalstage.Companies.Where((s => s.comp_idnr == Company.comp_idnr)).Take(1).ToList();
                if (singleuniqueref != null)
                {
                    foreach (var Unf in singleuniqueref)
                    {
                        legstts = Unf.comp_legal_status;
                    }
                }
            }

            if (legstts != null)
            {

                using (var comlegalstage_name = new CDHCRM2MIPCLEntities())
                {

                    var get_legatstatusidm = comlegalstage_name.GetCaptureLegalstats4MIP(legstts).FirstOrDefault();
                    if (get_legatstatusidm != null)
                    {
                        MappingIdmmip idmmip = new MappingIdmmip();
                        IDM_MIP_DXchangerEntities entitystage = new IDM_MIP_DXchangerEntities();

                        if (Company.comp_legal_status != null)
                        {
                            return idmmip.Maaping_MIP_Field_to_IDM_Field(get_legatstatusidm);
                        }
                        else
                        {
                            return "";
                        }
                    }
                    return "";
                }

            }
            return "";
        }


        private static string ClientLegalstatus(usp_LAW_SelectCompanyResult Company)
        {

            string legstts = null;
            using (var comlegalstage = new CDHCRM2MIPCLEntities())
            {

                var singleuniqueref = comlegalstage.Companies.Where((s => s.comp_idnr == Company.comp_idnr)).Take(1).ToList();
                if (singleuniqueref != null)
                {
                    foreach (var Unf in singleuniqueref)
                    {
                        legstts = Unf.comp_legal_status;
                    }
                }
            }

            if (legstts != null)
            {

                using (var comlegalstage_name = new CDHCRM2MIPCLEntities())
                {

                    var get_legatstatusidm = comlegalstage_name.GetCaptureLegalstats4MIP(legstts).FirstOrDefault();
                    if (get_legatstatusidm != null)
                    {
                        MappingIdmmip idmmip = new MappingIdmmip();
                        IDM_MIP_DXchangerEntities entitystage = new IDM_MIP_DXchangerEntities();

                        if (Company.comp_legal_status != null)
                        {
                            return idmmip.Maaping_MIP_Field_to_IDM_Field(get_legatstatusidm);
                        }
                        else
                        {
                            return "";
                        }
                    }
                    return "";
                }

            }
            return "";
        }


        private static string OppoStage_old(usp_LAW_SelectOpportunity Oppertunity)
        {
            MappingIdmmip idmmip = new MappingIdmmip();
            if (Oppertunity.Oppo_Stage != null)
            {
                return idmmip.Maaping_MIP_Field_to_IDM_Field(Oppertunity.Oppo_Stage.Trim());
            }
            else
            {
                return "Accepted";
            }

            //MappingIdmmip idmmip = new MappingIdmmip();
            //string OppoStages = null;

            //if (Oppertunity.Oppo_Stage.Trim() == "NCTCOG")
            //{
            //    OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("NCTCOG");
            //    // OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("NCT");
            //}
            //else if (Oppertunity.Oppo_Stage.Trim() == "SavingSheet")
            //{
            //    OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("Negotiations Finalised");
            //}
            //else if (Oppertunity.Oppo_Stage.Trim() == "DCRS")
            //{
            //    OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("Assigned to Negotiations");
            //}
            //else if (Oppertunity.Oppo_Stage.Trim() == "NCTComplete")
            //{
            //    OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("CPs Notified NCT Application");
            //}
            //else if (Oppertunity.Oppo_Stage.Trim() == "NCTComplete")
            //{
            //    OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("CPs Notified NCT Application");
            //}
            //else if (Oppertunity.Oppo_Stage.Trim() == "NCTComplete")
            //{
            //    OppoStages = idmmip.Maaping_MIP_Field_to_IDM_Field("CPs Notified NCT Application");
            //}
            //else
            //{
            //    OppoStages = Oppertunity.Oppo_Stage.Trim();
            //}
            //return OppoStages;
        }

        private static string Getadreedd(usp_AddressByAddressIdResult Address)
        {
            string address1;

            #region Adresses
            if (Address.Addr_Address1 != null)
                return address1 = Address.Addr_Address1.Substring(0, 30).Trim();
            return "";

            #endregion Adreses
        }
        private static string Getadreedd2(usp_AddressByAddressIdResult Address)
        {
            string address2;

            #region Adresses
            if (Address.Addr_Address2 != null)
                return address2 = Address.Addr_Address2.Substring(0, 30).Trim();
            else return "";

            #endregion Adreses
        }

        private static string Getadreedd3(usp_AddressByAddressIdResult Address)
        {
            string address3;

            #region Adresses
            if (Address.Addr_Address3 != null)
                return address3 = Address.Addr_Address3?.Substring(0, 30).Trim();
            return "";

            #endregion Adreses
        }

        private static string Getadreedd4(usp_AddressByAddressIdResult Address)
        {
            string address4;
            #region Adresses

            if (Address.Addr_Address4 != null)
                return address4 = Address.Addr_Address4.Substring(0, 30).Trim();
            return "";
            #endregion Adreses
        }

        private static sbyte Getnubdeb(usp_LAW_SelectCompanyResult Company)
        {
            sbyte comp_dependants = 0;
            if (Company.comp_dependants == null)
            {
                comp_dependants = 0;
            }
            else
            {
                comp_dependants = sbyte.Parse(Company.comp_dependants);
            }

            return comp_dependants;
        }

        private static string Getcenm(usp_LAW_SelectCompanyResult Company)
        {
            string cellnum;
            if (Company.comp_dummy25 != null)
            {
                cellnum = Company.comp_dummy25.Trim();
            }
            else
            {
                cellnum = "";
            }

            return cellnum;
        }

        private static string GetMaritalstat(usp_LAW_SelectCompanyResult Company)
        {
            string maritalstatus;
            if (Company.comp_maritalStatus != null)
            {
                if (Company.comp_maritalStatus.ToLower().Trim() == "single")
                {
                    maritalstatus = "er_AcStaMarSin";
                }
                else if (Company.comp_maritalStatus.ToLower().Trim() == "cop")
                {
                    maritalstatus = "er_AcStaMarCOM";
                }
                else if (Company.comp_maritalStatus.ToLower().Trim() == "anc")
                {
                    maritalstatus = "er_AcStaMarANC";
                }
                else if (Company.comp_maritalStatus.ToLower().Trim() == "divorced")
                {
                    maritalstatus = "er_AcStaMarDiv ";
                }
                else if (Company.comp_maritalStatus.ToLower().Trim() == "widowed")
                {
                    maritalstatus = "er_AcStaMarWid";
                }
                else
                {
                    maritalstatus = "er_AcStaMarSin";
                }
            }
            else
            {
                maritalstatus = "er_AcStaMarSin";
            }

            return maritalstatus;
        }

        private static string Getgender(usp_LAW_SelectCompanyResult Company)
        {
            string gednder = null;

            if (Company?.comp_gender == ("female").ToLower())
            {
                gednder = "er_AcPerGenFem";
            }
            else if (Company?.comp_gender == ("male").ToLower())
            {
                gednder = "er_AcPerGenMal";
            }
            else
            {
                gednder = "";
            }

            return gednder;
        }

        private static string Getrace(usp_LAW_SelectCompanyResult Company)
        {
            string comp_race = null;

            if (Company.comp_race.ToLower().Trim() == "asian")
            {
                comp_race = "er_AcPerRaceInd";
            }
            else if (Company.comp_race.ToLower().Trim() == "black")
            {
                comp_race = "er_AcPerRaceBlk";
            }
            else if (Company.comp_race.ToLower().Trim() == "coloured")
            {
                comp_race = "er_AcPerRaceClr";
            }
            else if (Company.comp_race.ToLower().Trim() == "white")
            {
                comp_race = "er_AcPerRaceWht";
            }
            else if (Company.comp_race.ToLower().Trim() == "indian")
            {
                comp_race = "er_AcPerRaceInd";
            }
            else { comp_race = null; }

            return comp_race;
        }

        private static DateTime GetDoB(usp_LAW_SelectCompanyResult Company)
        {
            int year;
            int month;
            int day;
            DateTime dob;

            try
            {
                if (Company.comp_dob != null)
                {
                    dob = Company.comp_dob.Value;
                }
                else
                {
                    //Log("11.3.2");
                    year = int.Parse(Company.comp_idnr.Substring(0, 2));
                    month = int.Parse(Company.comp_idnr.Substring(2, 2));
                    day = int.Parse(Company.comp_idnr.Substring(4, 2));

                    if ((int.Parse(DateTime.Now.Year.ToString().Substring(2, 2)) - year) > 0)
                    {
                        year = year + 2000;
                    }
                    else
                    {
                        year = year + 1900;
                    }
                    dob = new DateTime(year, month, day);
                }
            }
            catch
            {
                throw new Exception("Could not populate Date Of Birth");
            }

            return dob;
        }

        private static string Getfirsname(usp_LAW_SelectCompanyResult Company)
        {
            string firstnames = null;
            if (Company.Comp_Name != null)
            {
                if (Company.Comp_Name.Trim().Contains(' '))
                {
                    firstnames = Company.Comp_Name.Trim().Substring(0, Company.Comp_Name.Trim().IndexOf(' '));
                }
                else
                {
                    firstnames = Company.Comp_Name.Trim();
                }
            }

            return firstnames;
        }

        private static string GetLastName(usp_LAW_SelectCompanyResult Company)
        {
            string lastname = null;
            if (Company.Comp_Name != null)
            {
                if (Company.Comp_Name.Trim().Contains(' '))
                {
                    lastname = Company.Comp_Name.Trim().Substring(1, Company.Comp_Name.Trim().IndexOf(' ') +1);
             
                    

                    string fullName = Company.Comp_Name.Trim();
                    string[] names = fullName.Split(' ');
                    //string name = names.First();
                    lastname = names.Last();

                    return lastname;

                }
                else
                {
                    lastname = Company.Comp_Name.Trim();
                }
            }

            return lastname;
        }
        #endregion


        #endregion
    }
}

