﻿//Code implementeed by Fannie Makhokha
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceProcess;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Timers;
using System.Threading;
using System.Configuration;
using IDMLogging;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using CDHCL2MIP.IDMDXchanger.failedlead;
using CDHCL2MIP.IDMDXchanger;
using log4net;

namespace CDHCL2MIP.JsonBuilder
{
    public class ProcessDBToMipclient
    {
        private JavaScriptSerializer js = new JavaScriptSerializer();
        MappingIdmmip idmmip = new MappingIdmmip();
        ILog log = LogManager.GetLogger(typeof(ProcessDBToMipclient));
        public string StartingMainCode_ForBulkFile(List<int> clientID_oppo_)
        {
            BitechConsumerExportTEntities1 entities1_mip = new BitechConsumerExportTEntities1();
            #region Declarations
            int seconds;
            decimal kudjointval;
            int C_Lead;
            decimal nonkudjointval;

            string secondsdelay = ConfigurationManager.AppSettings["secondsdelay"];
            string kudjoint = ConfigurationManager.AppSettings["kudjoint"];
            string nonkudjoint = ConfigurationManager.AppSettings["nonkudjoint"];

            seconds = Convert.ToInt32(secondsdelay);
            kudjointval = Convert.ToDecimal(kudjoint);
            nonkudjointval = Convert.ToDecimal(nonkudjoint);
            LoggingErrorsandData loggingErrorsandData = new LoggingErrorsandData();
            #endregion

            Root clients = null;
            Thread.Sleep(1);
            {
                try
                {

                    CRMDataDataContext crmData = new CRMDataDataContext();

                    int? clientID_oppo;

                    cdhclient cdhclient = new cdhclient();
                    string jsonData = null;
                    #region main ConsumerDetect

                    foreach (var clientID_oppo__ in clientID_oppo_)
                    {
                        clientID_oppo = (int)clientID_oppo__;
                        try
                        {

                            #region CRM Data Context data null Verifications
                            try
                            {
                                var OppertunityCheckNull = crmData.usp_LAW_SelectOpportunity(clientID_oppo).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                //loggingErrorsandData.WriteErrorLog("");
                                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                                {
                                    throw new Exception("Oppertunity data incomplete, Verify Oppertunity data and resubmit again for : " + clientID_oppo);
                                }
                                else
                                {
                                    throw new Exception("Oppertunity data incomplete, Verify Oppertunity data and resubmit again for : " + clientID_oppo);
                                }
                            }
                            usp_LAW_SelectOpportunity Oppertunity = crmData.usp_LAW_SelectOpportunity(clientID_oppo).FirstOrDefault();
                            C_Lead = Oppertunity.Oppo_OpportunityId;

                            try
                            {
                                var CheckNullLead = crmData.usp_LAW_SelectLead(Oppertunity.Oppo_LeadID).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                                {
                                    throw new Exception("Lead data incomplete, Verify Lead data and resubmit again for : " + clientID_oppo);
                                }
                                else
                                {
                                    throw new Exception("Lead data incomplete, Verify Lead data and resubmit again for : " + clientID_oppo);
                                }
                            }
                            usp_LAW_SelectLeadResult Lead = crmData.usp_LAW_SelectLead(Oppertunity.Oppo_LeadID).FirstOrDefault();



                            try
                            {
                                var ChecNullCompany = crmData.usp_LAW_SelectCompany(Oppertunity.Oppo_PrimaryCompanyId).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                                {
                                    throw new Exception("Company data incomplete, Verify Company data and resubmit again for : " + clientID_oppo);
                                }
                                else
                                {
                                    throw new Exception("Company data incomplete, Verify Company data and resubmit again for : " + clientID_oppo);
                                }
                            }
                            usp_LAW_SelectCompanyResult Company = crmData.usp_LAW_SelectCompany(Oppertunity.Oppo_PrimaryCompanyId).FirstOrDefault();

                            try
                            {
                                var AddressCheckNull = crmData.usp_AddressByAddressId(Company.Comp_PrimaryAddressId).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                                {
                                    throw new Exception("Adresses data incomplete, Verify Adresses data and resubmit again for : " + clientID_oppo);
                                }
                                else
                                {
                                    throw new Exception("Adresses data incomplete, Verify Adresses data and resubmit again for : " + clientID_oppo);
                                }
                            }
                            usp_AddressByAddressIdResult Address = crmData.usp_AddressByAddressId(Company.Comp_PrimaryAddressId).FirstOrDefault();

                            try
                            {
                                var BankCheckNull = crmData.usp_BankByCompanyId(Oppertunity.Oppo_PrimaryCompanyId).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                                {
                                    throw new Exception("Bank data incomplete, Verify Bank data and resubmit again for : " + clientID_oppo);
                                }
                                else
                                {
                                    throw new Exception("Bank data incomplete, Verify Bank data and resubmit again for : " + clientID_oppo);
                                }
                            }
                            usp_BankByCompanyIdResult Bank = crmData.usp_BankByCompanyId(Oppertunity.Oppo_PrimaryCompanyId).FirstOrDefault();


                            usp_LAW_SelectPersonResult Person = crmData.usp_LAW_SelectPerson(Oppertunity.Oppo_PrimaryPersonId).FirstOrDefault();

                            try
                            {
                                var CheckNullCell = crmData.usp_getCellNumber(Oppertunity.Oppo_PrimaryCompanyId).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                loggingErrorsandData.WriteErrorLog("Cell is null");
                                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                                {
                                    //throw new Exception("Cell data incomplete, Verify Cell data and resubmit again for : " + clientID_oppo);
                                }
                                else
                                {
                                    //throw new Exception("Cell data incomplete, Verify Cell data and resubmit again for : " + clientID_oppo);
                                }
                            }
                            usp_getCellNumberResult Cell = crmData.usp_getCellNumber(Oppertunity.Oppo_PrimaryCompanyId).FirstOrDefault();



                            #endregion



                            #endregion




                            #region Budgted builder
                            try
                            {
                                var BudCheckNull = crmData.usp_LAW_SelectOpportunity(clientID_oppo).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                //loggingErrorsandData.WriteErrorLog("");
                                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                                {
                                    throw new Exception("Budget data incomplete, Verify Budget data and resubmit again for : " + clientID_oppo);
                                }
                                else
                                {
                                    throw new Exception("Budget data incomplete, Verify Budget data and resubmit again for : " + clientID_oppo);
                                }
                            }
                            usp_SelectBudgetByOpportunityIdResult Bud = crmData.usp_SelectBudgetByOpportunityId(clientID_oppo).FirstOrDefault();



                            #endregion

                            #region Obligations space
                            //consumerImportXML.client.obligations = new List<ConsumerImportXML.rootClientObligation>();
                            try
                            {
                                var CheckNullLeadDBList = crmData.usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNew(clientID_oppo, Oppertunity.Oppo_LeadID, "").ToList();
                            }
                            catch (Exception ex)
                            {
                                //loggingErrorsandData.WriteErrorLog("");
                                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                                {
                                    throw new Exception("obligations data Not found, Verify obligations data and resubmit again for : " + clientID_oppo);
                                }
                                else if (ex.ToString().Contains("Subquery returned more than 1 value. This is not permitted when the subquery follows =, !=, <, <= , >, >= or when the subquery is used as an expression."))
                                {
                                    throw new Exception("There seem to be duplication of data on the this client, Verify obligations data and resubmit again for : " + clientID_oppo);
                                }
                                else
                                {
                                    throw new Exception("obligations data Not found, Verify obligations data and resubmit again for : " + clientID_oppo);
                                }
                            }

                            List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList = crmData.usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNew(clientID_oppo, Oppertunity.Oppo_LeadID, "").ToList();

                            usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp = crmData.usp_LAW_SelectLeadDBExpByLeadID(Lead.Lead_LeadID).FirstOrDefault();

                            #endregion


                            #region single id number
                            //Root fullsinglecdhclient = cdhclient.Root(Oppertunity, Company, Address, Bank, Person, Cell, Lead, Bud, LeadDBList, LeadDBExp);

                            //jsonData = js.Serialize(fullsinglecdhclient);
                            //return jsonData; 
                            #endregion

                            //QUEUED ID NUMBERS
                            log.Info(clientID_oppo);
                            Root fullsinglecdhclient = cdhclient.Root(Oppertunity, Company, Address, Bank, Person, Cell, Lead, Bud, LeadDBList, LeadDBExp);

                            JObject lead = JObject.FromObject(fullsinglecdhclient.dsFBAImportIDMData.ttLeadDetails?[0]);

                            if (clients == null)
                            {
                                clients = fullsinglecdhclient;
                            }
                            else
                            {
                                clients.dsFBAImportIDMData.ttLeadDetails.Add(JsonConvert.DeserializeObject<TtLeadDetail>(JsonConvert.SerializeObject(lead)));
                            }
                            //entities1_mip.Insertpassleadcdhtomip(C_Lead);
                            // entities1_mip.Removedpassleadcdhtomip(C_Lead);
                            jsonData = js.Serialize(clients);
                            //string fulljason = null;





                            //File.WriteAllText($@"D:\CDH Migration\{Guid.NewGuid()}_{DateTime.Now.ToString("yyy-MM-dd")}.json", jsonData);

                            //QUEUED ID NUMBERS
                        }


                        #region  catch (Exception exc)
                        catch (Exception exc)
                        {
                            Log("Main Try Catch statement : " + exc.Message.ToString() + "\n");
                        }
                    }

                    string pathlogs = ConfigurationManager.AppSettings["ProcesedClients"];
                    //string pathlogs = "ProcesedClients";
                    Guid filename = Guid.NewGuid();
                    string filenam = filename.ToString();
                    try
                    {
                        File.WriteAllText(@"" + pathlogs + filenam + ".json", jsonData);
                    }
                    catch (Exception ex)
                    {

                    }
                    finally { }

                    #endregion
                    //File.WriteAllText($@"C:\Users\fanniem\source\repos\Projects\##11Clinet_MIP_TO_IDM_Update\Client_MIP_TO_IDM_Update\{Guid.NewGuid()}_{DateTime.Now.ToString("yyy-MM-dd")}.json", jsonData);

                    return jsonData;
                }

                catch (Exception exc)
                {
                    Log("Main Try Catch statement : " + exc.Message.ToString() + "\n");
                    Log("Main Try Catch statement exc : " + exc.ToString());
                    return null;
                }

            }
        }
        #region Logging info and Errors
        public static void Log(string msg)
        {
            LoggingErrorsandData loggingErrorsandData = new LoggingErrorsandData();
            try
            {
                loggingErrorsandData.WriteErrorLog(msg);
            }
            catch (Exception ed)
            {
                loggingErrorsandData.WriteErrorLog("Fail to log" + ed.Message.ToString());
            }
        }
        #endregion

    }
}

