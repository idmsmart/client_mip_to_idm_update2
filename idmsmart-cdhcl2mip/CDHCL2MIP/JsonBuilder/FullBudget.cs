﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDHCL2MIP.IDMDXchanger;

namespace CDHCL2MIP.JsonBuilder
{
    public class FullBudget
    {
        CRMDataDataContext crmData = new CRMDataDataContext();
        MappingIdmmip idmmip = new MappingIdmmip();
        #region Budget and child 

        #region IncomeItem
        public List<IncomeItem> IncomeItem(usp_LAW_SelectLeadResult Lead)
        {
            IncomeItem _IncomeItem = null;
            int c = 1;
            List<IncomeItem> allIncomeItem = new List<IncomeItem>();

            #region Incone ground
            try
            {
                var CheckNullLeadDBExp = crmData.usp_LAW_SelectLeadDBExpByLeadID(Lead.Lead_LeadID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Object reference not set to an instance of an object"))
                {
                    throw new Exception("LeadDBExp data Not found, Verify LeadDBExp data and resubmit again for : " + Lead.Lead_LeadID);
                }
                else
                {
                    throw new Exception("LeadDBExp data Not found, Verify LeadDBExp data and resubmit again for : " + Lead.Lead_LeadID);
                }
            }
            usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp = crmData.usp_LAW_SelectLeadDBExpByLeadID(Lead.Lead_LeadID).FirstOrDefault();

            try
            {
                if (Convert.ToString(LeadDBExp.lede_grosssalary.GetValueOrDefault(0)) != null && Convert.ToString(LeadDBExp.lede_grosssalaryc.GetValueOrDefault(0)) != null)
                {
                    if (LeadDBExp.lede_grosssalary.GetValueOrDefault(0) + LeadDBExp.lede_grosssalaryc.GetValueOrDefault(0) > 0)
                    {
                        _IncomeItem = new IncomeItem
                        {
                            BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                            Value = (double)LeadDBExp.lede_grosssalary.GetValueOrDefault(0),
                            Sequence = c,
                            FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly"),
                            Narration = "Gross Pay",
                            TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field("Salary")

                        };
                        allIncomeItem.Add(_IncomeItem);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (Convert.ToString(LeadDBExp.lede_overtime.GetValueOrDefault(0)) != null && Convert.ToString(LeadDBExp.lede_overtimec.GetValueOrDefault(0)) != null)
                {
                    if (LeadDBExp.lede_overtime.GetValueOrDefault(0) + LeadDBExp.lede_overtimec.GetValueOrDefault(0) > 0)
                    {
                        _IncomeItem = new IncomeItem
                        {
                            BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                            Value = (double)LeadDBExp.lede_overtime.GetValueOrDefault(0),
                            Sequence = c++,
                            FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly"),
                            Narration = "Overtime",
                            TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field("Overtime")

                        };
                        allIncomeItem.Add(_IncomeItem);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            try
            {
                if (Convert.ToString(LeadDBExp.lede_otherincome.GetValueOrDefault(0)) != null && Convert.ToString(LeadDBExp.lede_otherincomec.GetValueOrDefault(0)) != null)
                {
                    if (LeadDBExp.lede_otherincome.GetValueOrDefault(0) + LeadDBExp.lede_otherincomec.GetValueOrDefault(0) > 0)
                    {
                        _IncomeItem = new IncomeItem
                        {
                            BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                            Value = (double)LeadDBExp.lede_otherincome.GetValueOrDefault(0),
                            Sequence = c++,
                            FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly"),
                            Narration = idmmip.Maaping_MIP_Field_to_IDM_Field("Other Income"),
                            TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field("Other Income")

                        };
                        allIncomeItem.Add(_IncomeItem);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log("Other Income missing " + ex);
            }
            try
            {
                if (Convert.ToString(LeadDBExp.lede_otherincome2.GetValueOrDefault(0)) != null && Convert.ToString(LeadDBExp.lede_otherincomec2.GetValueOrDefault(0)) != null)
                {
                    if (LeadDBExp.lede_otherincome2.GetValueOrDefault(0) + LeadDBExp.lede_otherincomec2.GetValueOrDefault(0) > 0)
                    {
                        _IncomeItem = new IncomeItem
                        {
                            BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                            Value = (double)LeadDBExp.lede_otherincome2.GetValueOrDefault(0),
                            Sequence = c++,
                            FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly"),
                            Narration = LeadDBExp.lede_otherincome2desc,
                            TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field("Other Income")

                        };
                        allIncomeItem.Add(_IncomeItem);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log("lede_otherincome2 missing" + ex);
            }
            #endregion

            return allIncomeItem;
        }

        #endregion

        #region ExpenseItem
        public List<ExpenseItem> ExpenseItem(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp)
        {
            ExpenseItem _ExpenseItem = null;
            List<ExpenseItem> allExpenseItem = new List<ExpenseItem>();
            #region Expenses ground
            int cnt = 1;
            if ((double)LeadDBExp.lede_houserent.GetValueOrDefault(0) + (double)LeadDBExp.lede_houserentc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_houserentnotes) ? "" : LeadDBExp.lede_houserentnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field("Rent"),
                    Value = (double)LeadDBExp.lede_houserent.GetValueOrDefault(0) + (double)LeadDBExp.lede_houserentc.GetValueOrDefault(0),
                    Narration = "Rent",
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt
                }; allExpenseItem.Add(_ExpenseItem);

            }

            if ((double)LeadDBExp.lede_water.GetValueOrDefault(0) + (double)LeadDBExp.lede_waterc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_waternotes) ? "" : LeadDBExp.lede_waternotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Water & Electricity",
                    Value = (double)LeadDBExp.lede_water.GetValueOrDefault(0) + (double)LeadDBExp.lede_waterc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpWatElec",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_Security.GetValueOrDefault(0) + (double)LeadDBExp.lede_securityc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_securitynotes) ? "" : LeadDBExp.lede_securitynotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Security",
                    Value = (double)LeadDBExp.lede_Security.GetValueOrDefault(0) + (double)LeadDBExp.lede_securityc.GetValueOrDefault(0),
                    TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field("Security"),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_Petrol.GetValueOrDefault(0) + (double)LeadDBExp.lede_petrolc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_petrolnotes) ? "" : LeadDBExp.lede_petrolnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Petrol/Travel",
                    Value = (double)LeadDBExp.lede_Petrol.GetValueOrDefault(0) + (double)LeadDBExp.lede_petrolc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpPetTravel",//idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_insurance.GetValueOrDefault(0) + (double)LeadDBExp.lede_insurancec.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_insurancenotes) ? "" : LeadDBExp.lede_insurancenotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Car&House insurance",
                    Value = (double)LeadDBExp.lede_insurance.GetValueOrDefault(0) + (double)LeadDBExp.lede_insurancec.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpHouseInsu",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_lifeinsurance.GetValueOrDefault(0) + (double)LeadDBExp.lede_lifeinsurancec.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_lifeinsurancenotes) ? "" : LeadDBExp.lede_lifeinsurancenotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Life Insurance",
                    Value = (double)LeadDBExp.lede_lifeinsurance.GetValueOrDefault(0) + (double)LeadDBExp.lede_lifeinsurancec.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpLifeInsur",//idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_groceries.GetValueOrDefault(0) + (double)LeadDBExp.lede_groceriesc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_groceriesnotes) ? "" : LeadDBExp.lede_groceriesnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Groceries",
                    Value = (double)LeadDBExp.lede_groceries.GetValueOrDefault(0) + (double)LeadDBExp.lede_groceriesc.GetValueOrDefault(0),
                    TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field("Groceries"),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_clothing.GetValueOrDefault(0) + (double)LeadDBExp.lede_clothingc.GetValueOrDefault(0) > 0)
            {
                //string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_childmaintnotes) ? "" : LeadDBExp.lede_childmaintnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Clothing",
                    Value = (double)LeadDBExp.lede_clothing.GetValueOrDefault(0) + (double)LeadDBExp.lede_clothingc.GetValueOrDefault(0),
                    TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field("Clothing"),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_childmaint.GetValueOrDefault(0) + (double)LeadDBExp.lede_childmaintc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_childmaintnotes) ? "" : LeadDBExp.lede_childmaintnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Child maintenance",
                    Value = (double)LeadDBExp.lede_childmaint.GetValueOrDefault(0) + (double)LeadDBExp.lede_childmaintc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpChldMaint",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_homemaint.GetValueOrDefault(0) + (double)LeadDBExp.lede_homemaintcc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_homemaintnotes) ? "" : LeadDBExp.lede_homemaintnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "house maintenance",
                    Value = (double)LeadDBExp.lede_homemaint.GetValueOrDefault(0) + (double)LeadDBExp.lede_homemaintcc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpHomeMaint",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_vehiclemaint.GetValueOrDefault(0) + (double)LeadDBExp.lede_vehiclemaintc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_vehiclemaintnotes) ? "" : LeadDBExp.lede_vehiclemaintnotes.Trim(); 
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Vehicle Maintenance",
                    Value = (double)LeadDBExp.lede_vehiclemaint.GetValueOrDefault(0) + (double)LeadDBExp.lede_vehiclemaintc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpVehMaint",//idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_medaid2.GetValueOrDefault(0) + (double)LeadDBExp.lede_medaid2c.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_medaid2notes) ? "" : LeadDBExp.lede_medaid2notes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Medical expense",
                    Value = (double)LeadDBExp.lede_medaid2.GetValueOrDefault(0) + (double)LeadDBExp.lede_medaid2c.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpMedExpens",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_cellphone.GetValueOrDefault(0) + (double)LeadDBExp.lede_cellphonec.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_cellphonenotes) ? "" : LeadDBExp.lede_cellphonenotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Cellphone",
                    Value = (double)LeadDBExp.lede_cellphone.GetValueOrDefault(0) + (double)LeadDBExp.lede_cellphonec.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpCellphone",//idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_telephone.GetValueOrDefault(0) + (double)LeadDBExp.lede_telephonec.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_telephonenotes) ? "" : LeadDBExp.lede_telephonenotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Telephone",
                    Value = (double)LeadDBExp.lede_telephone.GetValueOrDefault(0) + (double)LeadDBExp.lede_telephonec.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpCellphone",//idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_lifeinsurepension.GetValueOrDefault(0) + (double)LeadDBExp.lede_lifeinsurepensionc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_lifeinsurepensionnotes) ? "" : LeadDBExp.lede_lifeinsurepensionnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Insurance - life",
                    Value = (double)LeadDBExp.lede_lifeinsurepension.GetValueOrDefault(0) + (double)LeadDBExp.lede_lifeinsurepensionc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpLifeInsur",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_tvlic.GetValueOrDefault(0) + (double)LeadDBExp.lede_tvlicc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_tvlicnotes) ? "" : LeadDBExp.lede_tvlicnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Tv license",
                    Value = (double)LeadDBExp.lede_tvlic.GetValueOrDefault(0) + (double)LeadDBExp.lede_tvlicc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpTVLicense",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_dstv.GetValueOrDefault(0) + (double)LeadDBExp.lede_dstvc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_dstvnotes) ? "" : LeadDBExp.lede_dstvnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "dstv",
                    Value = (double)LeadDBExp.lede_dstv.GetValueOrDefault(0) + (double)LeadDBExp.lede_dstvc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpDSTV",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_adsl.GetValueOrDefault(0) + (double)LeadDBExp.lede_adslc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_adslnotes) ? "" : LeadDBExp.lede_adslnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "adsl/internet",
                    Value = (double)LeadDBExp.lede_adsl.GetValueOrDefault(0) + (double)LeadDBExp.lede_adslc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpInternet",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_garden.GetValueOrDefault(0) + (double)LeadDBExp.lede_gardenc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_gardennotes) ? "" : LeadDBExp.lede_gardennotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "garden services",
                    Value = (double)LeadDBExp.lede_garden.GetValueOrDefault(0) + (double)LeadDBExp.lede_gardenc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpGarden",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_domestic.GetValueOrDefault(0) + (double)LeadDBExp.lede_domesticc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_domesticnotes) ? "" : LeadDBExp.lede_domesticnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Domestic",
                    Value = (double)LeadDBExp.lede_domestic.GetValueOrDefault(0) + (double)LeadDBExp.lede_domesticc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpDomWorker",//idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_school.GetValueOrDefault(0) + (double)LeadDBExp.lede_schoolc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_schoolnotes) ? "" : LeadDBExp.lede_schoolnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "School fees",
                    Value = (double)LeadDBExp.lede_school.GetValueOrDefault(0) + (double)LeadDBExp.lede_schoolc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpSchoolFee",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_petfood.GetValueOrDefault(0) + (double)LeadDBExp.lede_petfoodc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_petfoodnotes) ? "" : LeadDBExp.lede_petfoodnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Pet Food",
                    Value = (double)LeadDBExp.lede_petfood.GetValueOrDefault(0) + (double)LeadDBExp.lede_petfoodc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpOther",//idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_bankcharges.GetValueOrDefault(0) + (double)LeadDBExp.lede_bankchargesc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_bankchargesnotes) ? "" : LeadDBExp.lede_bankchargesnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Bank Charges",
                    Value = (double)LeadDBExp.lede_bankcharges.GetValueOrDefault(0) + (double)LeadDBExp.lede_bankchargesc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpBankCharg",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_creche.GetValueOrDefault(0) + (double)LeadDBExp.lede_crechec.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_crechenotes) ? "" : LeadDBExp.lede_crechenotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "creche",
                    Value = (double)LeadDBExp.lede_creche.GetValueOrDefault(0) + (double)LeadDBExp.lede_crechec.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpCreche",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_levies.GetValueOrDefault(0) + (double)LeadDBExp.lede_leviescc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_leviesnotes) ? "" : LeadDBExp.lede_leviesnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Levies",
                    Value = (double)LeadDBExp.lede_levies.GetValueOrDefault(0) + (double)LeadDBExp.lede_leviescc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpLevies", //idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_contingency.GetValueOrDefault(0) + (double)LeadDBExp.lede_Contigencyc.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_houserentnotes) ? "" : LeadDBExp.lede_houserentnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Contingency",
                    Value = (double)LeadDBExp.lede_contingency.GetValueOrDefault(0) + (double)LeadDBExp.lede_Contigencyc.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpContingen", //idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_otherexp1.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp1c.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_otherexp1notes) ? "" : LeadDBExp.lede_otherexp1notes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = LeadDBExp.lede_otherexp1desc ?? "other expense 1",
                    Value = (double)LeadDBExp.lede_otherexp1.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp1c.GetValueOrDefault(0),
                    TemplateItemCode = idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_otherexp2.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp2c.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_otherexp2notes) ? "" : LeadDBExp.lede_otherexp2notes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = LeadDBExp.lede_otherexp2desc ?? "other expense 2",
                    Value = (double)LeadDBExp.lede_otherexp2.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp2c.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpOther",//idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_otherexp3.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp3c.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_otherexp3notes) ? "" : LeadDBExp.lede_otherexp3notes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = LeadDBExp.lede_otherexp3desc ?? "other expense 3",
                    Value = (double)LeadDBExp.lede_otherexp3.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp3c.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpOther",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_otherexp4.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp4c.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_otherexp4notes) ? "" : LeadDBExp.lede_otherexp4notes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = LeadDBExp.lede_otherexp4desc ?? "other expense 4",
                    Value = (double)LeadDBExp.lede_otherexp4.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp4c.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpOther",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_otherexp5.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp5c.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_otherexp5notes) ? "" : LeadDBExp.lede_otherexp5notes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = LeadDBExp.lede_otherexp5desc ?? "other expense 5",
                    Value = (double)LeadDBExp.lede_otherexp5.GetValueOrDefault(0) + (double)LeadDBExp.lede_otherexp5c.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpOther",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            if ((double)LeadDBExp.lede_afterschool.GetValueOrDefault(0) + (double)LeadDBExp.lede_afterschool.GetValueOrDefault(0) > 0)
            {
                string temcode = String.IsNullOrWhiteSpace(LeadDBExp.lede_afterschoolnotes) ? "" : LeadDBExp.lede_afterschoolnotes.Trim();
                _ExpenseItem = new ExpenseItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    Narration = "Ater School Activities",
                    Value = (double)LeadDBExp.lede_afterschool.GetValueOrDefault(0),// + (double)LeadDBExp.lede_afterschool.GetValueOrDefault(0),
                    TemplateItemCode = "budAffItmExpOther",// idmmip.Maaping_MIP_Field_to_IDM_Field(temcode),
                    FrequencyTypeKey = "er_AcBudgetItemFreqMonthly",
                    Sequence = cnt++
                }; allExpenseItem.Add(_ExpenseItem);
            }

            #endregion
       
            return allExpenseItem;
        }

        #endregion

        #region AssetItem
        public List<AssetItem> AssetItem(usp_LAW_SelectLeadResult Lead)
        {
            AssetItem _AssetItem = null;

            List<AssetItem> allAssetItem = new List<AssetItem>();

            _AssetItem = new AssetItem
            {
                BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                TemplateItemCode = "",
                Sequence = 0,
                Narration = "",
                Value = 0.0,
                FrequencyTypeKey = ""
            };
            allAssetItem.Add(_AssetItem);
            return allAssetItem;
        }
        #endregion

        #region ObligationItem
        public List<ObligationItem> ObligationItem(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp)
        {
            ObligationItem _ObligationItem = null;

            List<ObligationItem> allObligationItem = new List<ObligationItem>();

            _ObligationItem = new ObligationItem
            {
                BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                Value = (double)LeadDBExp.lede_grosssalary.GetValueOrDefault(0),
                Sequence = 0,
                FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly"),
                Narration = "Gross Pay",
                TemplateItemCode = "Gross Pay"
            };
            allObligationItem.Add(_ObligationItem);
            
            return allObligationItem;
        }
        #endregion

        #region DeductionItem
        public List<DeductionItem> DeductionItem(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp)
        {
            DeductionItem _DeductionItem = null;

            List<DeductionItem> all_DeductionItem = new List<DeductionItem>();

            #region deductions Ground

            if (LeadDBExp.lede_tax.GetValueOrDefault(0) + LeadDBExp.lede_taxc.GetValueOrDefault(0) > 0)
            {
                _DeductionItem = new DeductionItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = "Statutory Deduction",
                    Sequence = 1,
                    Narration = "Tax",
                    Value = (double)LeadDBExp.lede_tax.GetValueOrDefault(0),
                    FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly")
                };
                all_DeductionItem.Add(_DeductionItem);
            }

            if (LeadDBExp.lede_medaid.GetValueOrDefault(0) + LeadDBExp.lede_medaidc.GetValueOrDefault(0) > 0)
            {
                _DeductionItem = new DeductionItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = "Medical Aid",
                    Sequence = 2,
                    Narration = "Medical Aid",
                    Value = (double)LeadDBExp.lede_medaid.GetValueOrDefault(0),
                  FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly")
                };
                all_DeductionItem.Add(_DeductionItem);
            }

            if (LeadDBExp.lede_pension.GetValueOrDefault(0) + LeadDBExp.lede_pensionc.GetValueOrDefault(0) > 0)
            {
                _DeductionItem = new DeductionItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = "Pension",
                    Sequence = 3,
                    Narration = "Pension",
                    Value = (double)LeadDBExp.lede_pension.GetValueOrDefault(0),
                  FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly")
                };
                all_DeductionItem.Add(_DeductionItem);
            }

            if (LeadDBExp.lede_uif.GetValueOrDefault(0) + LeadDBExp.lede_uifc.GetValueOrDefault(0) > 0)
            {
                _DeductionItem = new DeductionItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = "UIF",
                    Sequence = 4,
                    Narration = "UIF",
                    Value = (double)LeadDBExp.lede_uif.GetValueOrDefault(0),
                  FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly")
                };
                all_DeductionItem.Add(_DeductionItem);
            }

            if (LeadDBExp.lede_otherdeduct.GetValueOrDefault(0) > 0)
            {
                _DeductionItem = new DeductionItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = "Other",
                    Sequence = 5,
                    Narration = "Other",
                    Value = (double)LeadDBExp.lede_otherdeduct.GetValueOrDefault(0),
                  FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly")
                };
                all_DeductionItem.Add(_DeductionItem);
            }

            if (LeadDBExp.lede_otherdeductc.GetValueOrDefault(0) > 0)
            {
                _DeductionItem = new DeductionItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = LeadDBExp.lede_otherdeductcdesc,
                    Sequence = 6,
                    Narration = "Other",
                    Value = (double)LeadDBExp.lede_otherdeductc.GetValueOrDefault(0),
                  FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly")
                };
                all_DeductionItem.Add(_DeductionItem);
            }

            if (LeadDBExp.lede_otherdeduct2.GetValueOrDefault(0) > 0)
            {
                _DeductionItem = new DeductionItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = LeadDBExp.lede_otherdeduct2desc,
                    Sequence = 7,
                    Narration = "Other",
                    Value = (double)LeadDBExp.lede_otherdeduct2.GetValueOrDefault(0),
                  FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly")
                };
                all_DeductionItem.Add(_DeductionItem);
            }

            if (LeadDBExp.lede_otherdeductc2.GetValueOrDefault(0) > 0)
            {
                _DeductionItem = new DeductionItem
                {
                    BudgetReference = Convert.ToString(Lead.Lead_LeadID),
                    TemplateItemCode = "Other",
                    Sequence = 8,
                    Narration = "Other",
                    Value = (double)LeadDBExp.lede_otherdeductc2.GetValueOrDefault(0),
                  FrequencyTypeKey = idmmip.Maaping_MIP_Field_to_IDM_Field("Monthly")
                };
                all_DeductionItem.Add(_DeductionItem);
            }
            #endregion

            return all_DeductionItem;
        }
        #endregion

        #region Budget
        public List<Budget> Budget(usp_LAW_SelectLeadResult Lead, usp_SelectBudgetByOpportunityIdResult Bud, usp_LAW_SelectLeadDBExpByLeadIDResult LeadDBExp, List<usp_LAW_SelectLeadDBByOppoID_CreditProviderAllNewResult> LeadDBList)
        {
            Budget _Budget = null;

            List<Budget> allBudget = new List<Budget>();

            //get Captlede_dummy8 //Captlede_dummy9 with budgid

            _Budget = new Budget
            {
                LeadId = Convert.ToString(Lead.Lead_LeadID),
                Reference = Convert.ToString(Lead.Lead_LeadID),
                TotalIncome =(double) LeadDBExp.lede_grosssalary,
                TotalExpenses = (double)LeadDBExp.lede_totalexp,//  (double) Bud..lede_totalexp,
                TotalAssets = 0.0,//(double)LeadDBExp.a,
                TotalObligations = 0.0,//(double)LeadDBExp.le,
                TotalDeductions = 0.0,// (double)LeadDBExp.lede_dummy8,
                IncomeItem = IncomeItem(Lead),
                ExpenseItem = ExpenseItem(Lead, LeadDBExp),
                AssetItem = AssetItem(Lead),
                //ObligationItem = ObligationItem(Lead, LeadDBExp),
                DeductionItem = DeductionItem(Lead, LeadDBExp)
            };
            allBudget.Add(_Budget);
            return allBudget;
        }
        #endregion 
        #endregion

    }
}
