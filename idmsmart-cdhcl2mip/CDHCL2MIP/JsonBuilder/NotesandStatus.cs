﻿using CDHCL2MIP.ConsultantInformation;
using CDHCL2MIP.ConsultantInformation.CDHDB;
using CDHCL2MIP.IDMDXchanger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CDHCL2MIP.JsonBuilder
{
    public class NotesandStatus
    {
        MappingIdmmip idmmip = new MappingIdmmip();
        #region Budget
        //public List<Notes> CurNotes(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectCompanyResult Company)
        //{

        //    string _FAConsultantName = null;
        //    string _FCConsultantName = null;
        //    string _FCConsultantEmail = null;
        //    string _FCConsultantPhone = null;
        //    string _FCConsultantTeam = null;
        //    string CommentLabel = null;
        //    string CommentType = null;
        //    string CreatedDate = null;
        //    string Note = null;

        //    CDHConsultant cDHCL = new CDHConsultant();
        //    CDHCRM2MIPCLEntities cDHCRM = new CDHCRM2MIPCLEntities();
        //    var consult_details = cDHCL.RetreiveConsultantStats(Company.Comp_PrimaryUserId);
        //    var innotes = cDHCRM.usp_getnotesandstages(Lead.Lead_LeadID).ToList();


        //    foreach (var detailsdata in consult_details)
        //    {
        //        _FAConsultantName = String.IsNullOrEmpty(detailsdata.User_FirstName) ? "" : detailsdata.User_FirstName.Trim();
        //        _FCConsultantName = String.IsNullOrEmpty(detailsdata.User_LastName) ? "" : detailsdata.User_LastName.Trim();
        //        _FCConsultantEmail = String.IsNullOrEmpty(detailsdata.User_EmailAddress) ? "" : detailsdata.User_EmailAddress.Trim();
        //        _FCConsultantPhone = String.IsNullOrEmpty(detailsdata.User_Extension) ? "" : detailsdata.User_Extension.Trim();
        //        _FCConsultantTeam = String.IsNullOrEmpty(detailsdata.User_Department) ? "" : detailsdata.User_Department.Trim();
        //    }

        //    Notes _Notes = null;

        //    List<Notes> allNotes = new List<Notes>();

        //    int i = 1;
        //    foreach (var st in innotes)
        //    {

        //        Note = st.Lead_ProgressNote;
        //        //CreatedDate = Convert.ToString(st.Lead_CreatedDate); //not using this for now
        //        DateTime _crdt = (DateTime)st.Lead_CreatedDate;
        //        //DateTime _updt = (DateTime)st.Lead_UpdatedDate;
        //        CreatedDate = _crdt.ToString("yyyy-MM-dd");
        //        //UpdatedDate = Convert.ToString(_updt.ToString("yyyy-mm-dd"));
        //        _Notes = new Notes
        //        {
        //            LeadId = Convert.ToString(Lead.Lead_LeadID),
        //            Seq = i,
        //            CommentLabel = "Notes", // ,
        //            CommentType = st.Lead_Status,// "Notes",
        //            CreatedBy = _FCConsultantName + " " + _FAConsultantName,
        //            CreatedDate = CreatedDate,
        //            Note = Note

        //        };
        //        allNotes.Add(_Notes);
        //        i++;
        //    }
        //    return allNotes;
        //}
        #endregion


        public List<Notes> GetNotes(int leadid)
        {
            CDHCRM2MIPCLEntities _context = new CDHCRM2MIPCLEntities();
            var notes = _context.usp_getnotesandstages(leadid)
                .Select(a =>
                new Notes
                {
                    LeadId = a.Lead.ToString(),
                    Seq = Int32.Parse(a.Seq.ToString()),
                    Note = Regex.Replace(a.Note, @"\s+", string.Empty), 
                    CreatedDate = a.CreatedDate?.ToString("yyyy-MM-dd"),
                    CreatedBy = a.CreatedBy,
                    CommentType = "fin_AcDBTBApp",
                    CommentLabel = "Notes"
                }).ToList();

            return notes;
        }

        #region Budget
        public List<Stages> CurStages(usp_LAW_SelectLeadResult Lead, usp_LAW_SelectCompanyResult Company)
        {
            IDM_MIP_DXchangerEntities entitystage;//= new IDM_MIP_DXchangerEntities();

            //[comp_clientStatus]
            //[Oppo_Stage]

            string CreatedDate = null;
            string UpdatedDate = null;
            string l_progess = null;
            string Note = null;

            DateTime _crdt_n;
            DateTime _updt_n;
 
            Stages _Stages = null;
            string Stage_ = null;

        
            entitystage = new IDM_MIP_DXchangerEntities();
            var lead_progressdata = entitystage.get_leadProgressStage(Lead.Lead_LeadID).ToList();

            

            List<Stages> allStages = new List<Stages>();

            //lead
            int i = 1;

            if (lead_progressdata != null)
            {
                foreach (var l_progressdata in lead_progressdata)
                {
                    l_progess = l_progressdata.Lead_Stage;
                    DateTime _crdt = (DateTime)l_progressdata.Lead_CreatedDate;
                    DateTime _updt = (DateTime)l_progressdata.Lead_UpdatedDate;
                    CreatedDate = _crdt.ToString("yyyy-MM-dd");
                    UpdatedDate = _updt.ToString("yyyy-MM-dd");
                    Note = l_progess;
                    
                    
                    if (l_progess != null)
                    {
                    
                        if ((l_progess == ("Closed")) || (l_progess == ("ClosedMIP"))|| (l_progess == "ClosedCDH"))
                        {
                            l_progess = "CLS";
                            _Stages = new Stages
                            {
                                LeadId = Convert.ToString(Lead.Lead_LeadID),
                                Seq = i,
                                Stage = l_progess,
                                FromDate = CreatedDate,
                                ToDate = UpdatedDate,
                            };
                            allStages.Add(_Stages);
                            i++;
                        }
                        if ((l_progess == "NewLead") || (l_progess == "Transfer") || (l_progess == "") || (l_progess == string.Empty))
                        {
                            l_progess = "TRN";
                            _Stages = new Stages
                            {
                                LeadId = Convert.ToString(Lead.Lead_LeadID),
                                Seq = i,
                                Stage = l_progess,
                                FromDate = CreatedDate,
                                ToDate = UpdatedDate,
                            };
                            allStages.Add(_Stages);
                            i++;
                        }
                        else
                        {
                            entitystage = new IDM_MIP_DXchangerEntities();
                            var lead_progressdMApped_ = entitystage.get_leadProgressStagCaptureMapping(l_progess).FirstOrDefault();
                            
                            if (lead_progressdMApped_ != null)
                            {

                                Stage_ = idmmip.Maaping_MIP_Field_to_IDM_Field(lead_progressdMApped_);

                                if (Stage_ != null)
                                {

                                    if (Stage_ != null)
                                    {
                                        _Stages = new Stages
                                        {
                                            LeadId = Convert.ToString(Lead.Lead_LeadID),
                                            Seq = i,
                                            Stage = Stage_,
                                            FromDate = CreatedDate,
                                            ToDate = UpdatedDate,
                                        };
                                        allStages.Add(_Stages);
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }
                entitystage = new IDM_MIP_DXchangerEntities();
                var Oppo_Oppostatus = entitystage.get_Oppostage2(Lead.Lead_LeadID).ToList();
                if (Oppo_Oppostatus != null)
                {
                    foreach (var singleopp in Oppo_Oppostatus)
                    {
                        DateTime _crdt2 = (DateTime)singleopp.Oppo_Createddate;
                        DateTime _updt2 = (DateTime)singleopp.Oppo_Updateddate;
                        string CreatedDate2 = _crdt2.ToString("yyyy-MM-dd");
                        string UpdatedDate2 = _updt2.ToString("yyyy-MM-dd");
                        entitystage = new IDM_MIP_DXchangerEntities();
                        var Oppo_dMAppe_ = entitystage.get_OppoProgressStagCaptureMapping(Convert.ToString(singleopp.Oppo_Stage)).FirstOrDefault();
                        if (Oppo_dMAppe_ != null)
                        {
                            Stage_ = idmmip.Maaping_MIP_Field_to_IDM_Field(Oppo_dMAppe_);
                            if (Stage_ != null)
                            {
                                if (Stage_ != null)
                                {
                                    _Stages = new Stages
                                    {
                                        LeadId = Convert.ToString(Lead.Lead_LeadID),
                                        Seq = i,
                                        Stage = Stage_,
                                        FromDate = CreatedDate2,
                                        ToDate = UpdatedDate2,
                                    };
                                    allStages.Add(_Stages);
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            return allStages;
        }
        #endregion 

        public List<Stages> GetStages(int leadid)
        {
            IDM_MIP_DXchangerEntities entitystage = new IDM_MIP_DXchangerEntities();

            var stages = entitystage.get_LeadStageHistory(leadid).Select((a,i)=> 
            new Stages 
            {
                LeadId= a.LeadID.ToString(),
                Stage = a.Stage,
                Seq = i + 1,
                FromDate = a.FromDate?.ToString("yyyy-MM-dd"),
                ToDate = a.ToDate?.ToString("yyyy-MM-dd") 
            }).ToList();

          
            return stages;
        }
    }
}
