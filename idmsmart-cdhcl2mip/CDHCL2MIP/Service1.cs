﻿//Code implementeed by Fannie Makhokha
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceProcess;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Timers;
using System.Threading;
using System.Configuration;
using CDHCL2MIP.JsonBuilder;
using IDMLogging;

namespace CDHCL2MIP
{
    public partial class CDHCL2MIPService : ServiceBase
    {

        public CDHCL2MIPService()
        {
            InitializeComponent();
        }

        #region Startansstop
        protected override void OnStart(string[] args)
        {
            var myThread = new Thread(new ThreadStart(StartingMainCode));
            myThread.IsBackground = true;
            myThread.Start();
        }

        protected override void OnStop()
        {
            var myThread = new Thread(new ThreadStart(StartingMainCode));
            myThread.IsBackground = false;
            myThread.Abort();
        } 
        #endregion

        public void StartingMainCode()
        {
          
            ProcessCDHClient processModule = new ProcessCDHClient();

            //processModule.StartingMainCode1();
        }

        #region Logging info and Errors
        public static void Log(string msg)
        {
            LoggingErrorsandData loggingErrorsandData = new LoggingErrorsandData();
            try
            {
                loggingErrorsandData.WriteErrorLog(msg);
            }
            catch (Exception ed)
            {
                loggingErrorsandData.WriteErrorLog("Fail to log" + ed.Message.ToString());
            }
            finally
            {

            }
        }
        #endregion

        #region LogExceptionsOnXmbuilder
        public static void LogExceptionsOnXmbuilder(string msg)
        {
            LoggingErrorsandData loggingErrorsandData = new LoggingErrorsandData();
            try
            {
                loggingErrorsandData.WriteErrorLog(msg);
            }
            catch (Exception ed)
            {

            }
            finally
            {

            }
        }
        #endregion

        #region ErrorEmail
        public void ErrorEmail(int Oppertunity, string Message)
        {


            /*Message = "Oppertunity: " + Oppertunity + " failed" + Environment.NewLine + Environment.NewLine + Message; 


            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.Host = "smtp.intelligentdebtgroup.co.za";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("errorlog@debtbusters.co.za", "P@ssword123");

            MailMessage mm = new MailMessage("errorlog@debtbusters.co.za", "itsupport@debtbusters.co.za", "Bitech Customer Export Failure", Message);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            client.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
            delegate(object s, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors sslPolicyErrors)
            { return true; };

            client.Send(mm);*/

        }
        #endregion
    }
}
