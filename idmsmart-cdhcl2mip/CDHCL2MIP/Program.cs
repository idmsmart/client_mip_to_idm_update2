﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CDHCL2MIP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if(!DEBUG)
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new CDHCL2MIPService() 
            };
            ServiceBase.Run(ServicesToRun);
#else
            CDHCL2MIPService myServ = new CDHCL2MIPService();
            myServ.StartingMainCode();
            //myServ.timer_Elapsed;
#endif


        }
    }
}
