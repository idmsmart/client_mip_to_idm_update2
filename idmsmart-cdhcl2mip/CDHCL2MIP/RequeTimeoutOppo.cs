﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDMLogging;

namespace CDHCL2MIP
{
    public class RequeTimeoutOppo
    {
        public string ResubmitTimeout(int OppoID)
        {
            BitechConsumerExportTEntities BitechConsumerExportTEntities = new BitechConsumerExportTEntities();
            try
            {
                BitechConsumerExportTEntities.RequeueDetectionLayer(OppoID);
                return "Success";
            }
            catch (Exception ex)
            {
                LoggingErrorsandData loggingErrorsandData = new LoggingErrorsandData();
                loggingErrorsandData.WriteErrorLog(ex.Message.ToString());
                return "Fail" + ex;
            }
        }
    }
}
