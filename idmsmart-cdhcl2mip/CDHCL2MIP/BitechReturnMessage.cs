﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CDHCL2MIP
{
    public class BitechReturnMessage
    {
        //messages
        public string SingleMessageFromBitech(string BitResponse)
        {
            BitResponse = BitResponse.Replace("<root><Response>1</Response><Error>", "");
            BitResponse = BitResponse.Replace("</Error></root>", "");
            return BitResponse;
        }
    }
}
