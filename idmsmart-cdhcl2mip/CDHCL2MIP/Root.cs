﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDHCL2MIP
{
    public class IncomeItem
    {
        //
        public string BudgetReference { get; set; }
        public string TemplateItemCode { get; set; }
        public int Sequence { get; set; }
        public string Narration { get; set; }
        public double Value { get; set; }
        public string FrequencyTypeKey { get; set; }
    }

    public class ExpenseItem
    {
        public string BudgetReference { get; set; }
        public string TemplateItemCode { get; set; }
        public int Sequence { get; set; }
        public string Narration { get; set; }
        public double Value { get; set; }
        public string FrequencyTypeKey { get; set; }
    }

    public class AssetItem
    {
        public string BudgetReference { get; set; }
        public string TemplateItemCode { get; set; }
        public int Sequence { get; set; }
        public string Narration { get; set; }
        public double Value { get; set; }
        public string FrequencyTypeKey { get; set; }
    }

    public class ObligationItem
    {
        public string BudgetReference { get; set; }
        public string TemplateItemCode { get; set; }
        public int Sequence { get; set; }
        public string Narration { get; set; }
        public double Value { get; set; }
        public string FrequencyTypeKey { get; set; }
    }

    public class DeductionItem
    {
        public string BudgetReference { get; set; }
        public string TemplateItemCode { get; set; }
        public int Sequence { get; set; }
        public string Narration { get; set; }
        public double Value { get; set; }
        public string FrequencyTypeKey { get; set; }
    }

    public class Notes
    {
        public string LeadId { get; set; }
        public int Seq { get; set; }
        public string Note { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CommentType { get; set; }
        public string CommentLabel { get; set; }
    }


    public class Stages
    {
        public string LeadId { get; set; }
        public int Seq { get; set; }
        public string Stage { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        //public string CommentType { get; set; }
        //public string CommentLabel { get; set; }
    }






    public class Budget
    {
        public string LeadId { get; set; }
        public string Reference { get; set; }
        public double TotalIncome { get; set; }
        public double TotalExpenses { get; set; }
        public double TotalAssets { get; set; }
        public double TotalObligations { get; set; }
        public double TotalDeductions { get; set; }
        public List<IncomeItem> IncomeItem { get; set; }
        public List<ExpenseItem> ExpenseItem { get; set; }
        public List<AssetItem> AssetItem { get; set; }
        public List<ObligationItem> ObligationItem { get; set; }
        public List<DeductionItem> DeductionItem { get; set; }
    }

    public class CreditLife
    {
        public string AccountNumber { get; set; }
        public double CreditLifeTotalCover { get; set; }
        public double TotalPremium { get; set; }
        public bool NormalCreditLifeSold { get; set; }
        public bool RetrenchmentSold { get; set; }
        public double RetrenchmentAmount { get; set; }
        public double CreditLifePremium { get; set; }
        public bool EnhancedCreditLife { get; set; }
        public double CoverAmount { get; set; }
        public object EffectiveDate { get; set; }
    }

    public class Obligation
    {
        public string LeadId { get; set; }
        public string AccountNumber { get; set; }
        public string CreditorDescription { get; set; }
        public string PaymentProfileId { get; set; }
        public int Terms { get; set; }
        public int CurrentTerms { get; set; }
        public string AccountType { get; set; }
        public object InceptionDate { get; set; }
        public object ExpiryDate { get; set; }
        public double OriginalInstallment { get; set; }
        public double CurrentInstallment { get; set; }
        public double OriginalBalance { get; set; }
        public double CurrentBalance { get; set; }
        public object LastPayDate { get; set; }
        public string IncludeExcludeDebtReview { get; set; }
        public string RIDTrader { get; set; }
        public string Promoted { get; set; }
        public string COBReceived { get; set; }
        public string COBUpdated { get; set; }
        public object COBUpdatedDate { get; set; }
        public double InterestRate { get; set; }
        public double CurrentInterestRate { get; set; }
        public string HyphenCreditorLink { get; set; }
        public object DateOfDefault { get; set; }
        public double MonthlyFeesExclInsurance { get; set; }
        public double InsurancePremiums { get; set; }
        public double Insurance { get; set; }
        public string Status { get; set; }
        public List<CreditLife> CreditLife { get; set; }
    }

    public class LifeBeneficiary
    {
        public string PolicyNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public object BirthDate { get; set; }
        public string Gender { get; set; }
        public string MainClientIDNumber { get; set; }
        public string Relationship { get; set; }
        public double SharePercentage { get; set; }
        public string CreatedBy { get; set; }
        public bool IsMainBeneficiary { get; set; }
    }

    public class LifeCover
    {
        public string LeadId { get; set; }
        public string PolicyNumber { get; set; }
        public double CoverAmount { get; set; }
        public double Premium { get; set; }
        public double PermanentDisabilityAmount { get; set; }
        public double DreadDiseaseAmount { get; set; }
        public double IncomeReplacementAmount { get; set; }
        public string CreatedBy { get; set; }
        public List<LifeBeneficiary> LifeBeneficiary { get; set; }
    }

    public class FuneralBeneficiary
    {
        public string PolicyNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public object BirthDate { get; set; }
        public string Gender { get; set; }
        public string MainClientIDNumber { get; set; }
        public string Relationship { get; set; }
        public double SharePercentage { get; set; }
        public string CreatedBy { get; set; }
        public bool IsMainBeneficiary { get; set; }
    }

    public class FuneralCover
    {
        public string LeadId { get; set; }
        public string PolicyNumber { get; set; }
        public double CoverAmount { get; set; }
        public double Premium { get; set; }
        public object FuneralDate { get; set; }
        public object FuneralFirstPayDate { get; set; }
        public int PeopleCovered { get; set; }
        public int AgeOfOldest { get; set; }
        public List<FuneralBeneficiary> FuneralBeneficiary { get; set; }
    }

    public class CreditLifeIn
    {
        public string LeadId { get; set; }
        public string PolicyNumber { get; set; }
        public double CoverAmount { get; set; }
        public double RehabCovered { get; set; }
        public double CreditLifePremium { get; set; }
        public double RetrenchmentAmount { get; set; }
        public double TotalPremium { get; set; }
        public object EffectiveDate { get; set; }
    }

    public class EnhancedCreditLife
    {
        public string LeadId { get; set; }
        public string PolicyNumber { get; set; }
        public double CoverAmount { get; set; }
        public double RehabCovered { get; set; }
        public double CreditLifePremium { get; set; }
        public double RetrenchmentAmount { get; set; }
        public double TotalPremium { get; set; }
        public object EffectiveDate { get; set; }
    }

    public class Insurance
    {
        public string LeadId { get; set; }
        public List<LifeCover> LifeCover { get; set; }
        public List<FuneralCover> FuneralCover { get; set; }
        public List<CreditLifeIn> CreditLifeIns { get; set; }
       // public List<EnhancedCreditLife> EnhancedCreditLife { get; set; }
    }

    public class TtLeadDetail
    {
        public string LeadID { get; set; }
        public string CRMLeadID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public object BirthDate { get; set; }
        public string IDType { get; set; }
        public string IDNumber { get; set; }
        public string Race { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string Email { get; set; }
        public string CellphoneNumber { get; set; }
        public string TelephoneNumber { get; set; }
        public int NumberOfDependents { get; set; }
        public string ResidentialAddress1 { get; set; }
        public string ResidentialAddress2 { get; set; }
        public string ResidentialAddress3 { get; set; }
        public string ResidentialCity { get; set; }
        public string ResidentialProvince { get; set; }
        public string ResidentialZipCode { get; set; }
        public string PostalAddress { get; set; }
        public string PostalCity { get; set; }
        public string PostalProvince { get; set; }
        public string PostalZipCode { get; set; }
        public string Occupation { get; set; }
        public string JobDescription { get; set; }
        public int PayDate { get; set; }
        public string EmployerName { get; set; }
        public string EmployerAddress { get; set; }
        public string EmployerZipCode { get; set; }
        public string EmployerPhone { get; set; }
        public string EmployerRisk { get; set; }
        public string IncomeBracket { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        public string SpouseIDNumber { get; set; }
        public string SpouseEmail { get; set; }
        public string SpouseCellphoneNumber { get; set; }
        public string SpouseJobDescription { get; set; }
        public int SpousePayDate { get; set; }
        public string SpouseEmployerName { get; set; }
        public string SpouseEmployerAddress { get; set; }
        public string SpouseEmployerZipCode { get; set; }
        public string SpouseEmployerPhone { get; set; }
        public string SpouseRace { get; set; }
        public object SpouseBirthDate { get; set; }
        public string SpouseCreditScore { get; set; }
        public object CreatedDate { get; set; }
        public string SearchKeywords { get; set; }
        public string DeviceWhenQuery { get; set; }
        public string ReasonForEnquiry { get; set; }
        public string ApplicationType { get; set; }
        public object Form17_1Date { get; set; }
        public object Form17_2Date { get; set; }
        public string Form17_2Type { get; set; }
        public string RejectReason { get; set; }
        public object Form17_WDate { get; set; }
        public string Withdrawal_YN { get; set; }
        public string BankName { get; set; }
        public string BankBranchCode { get; set; }
        public string AccountType { get; set; }
        public string BankAccountNumber { get; set; }
        public string CollectionType { get; set; }
        public string BankAVSResult { get; set; }
        public string PaymentScheduleSent { get; set; }
        public object PaymentScheduleDate { get; set; }
        public object UpdatedDate { get; set; }
        public object DebitOrder1Date { get; set; }
        public object DebitOrder2Date { get; set; }
        public string CreditScore { get; set; }
        public string DCName { get; set; }
        public string DCNumber { get; set; }
        public string Stage { get; set; }
        public string Status { get; set; }
        public string Source { get; set; }
        public string WebSource { get; set; }
        public string Campaign { get; set; }
        public string FAConsultantName { get; set; }
        public string FCConsultantName { get; set; }
        public string FCConsultantEmail { get; set; }
        public string FCConsultantPhone { get; set; }
        public string FCConsultantTeam { get; set; }
        public double FirstPayment { get; set; }
        public double QuotedRehab { get; set; }
        public double FinalAgreedAmount { get; set; }
        public string DCRSScenario { get; set; }
        public double RestructureFee { get; set; }
        public double ApplicationFee { get; set; }
        public double LegalFee { get; set; }
        public double RecklessLendingFee { get; set; }
        public double AftercareFee { get; set; }
        public object WPSentDate { get; set; }
        public object WPSignedDate { get; set; }
        public object WPReceivedDate { get; set; }
        public string ProvisionalProposalSent { get; set; }
        public object ProvisionalProposalDateSent { get; set; }
        public string FinalProposalSent { get; set; }
        public object FinalProposalDateSent { get; set; }
        public string PreviousDebtCounselling { get; set; }
        public string LegalStatus { get; set; }
        public string IndustryRisk { get; set; }
        public string ClientAcceptance { get; set; }
        


        public List<Notes> Notes { get; set; }
        public List<Stages> Stages { get; set; }
        public List<Budget> Budget { get; set; }
        public List<Obligation> Obligation { get; set; }
        public List<Insurance> Insurance { get; set; }
    }

    public class DsFBAImportIDMData
    {
        public List<TtLeadDetail> ttLeadDetails { get; set; }
    }

    public class Root
    {
        public DsFBAImportIDMData dsFBAImportIDMData { get; set; }
    }


}
