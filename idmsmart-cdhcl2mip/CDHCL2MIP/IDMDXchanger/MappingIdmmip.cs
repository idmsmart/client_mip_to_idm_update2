﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDHCL2MIP.IDMDXchanger
{
    public class MappingIdmmip
    {
        public string Maaping_MIP_Field_to_IDM_Field(string IDM_strVal)
        {
            if (string.IsNullOrEmpty(IDM_strVal)){ return ""; }
            IDM_MIP_DXchangerEntities iDM_MIP_D = new IDM_MIP_DXchangerEntities();
            try
            {
                var Mapperfiled = (from field_records in iDM_MIP_D.Full_MIP_IDM_Data_Mapper
                                   where field_records.IDM_strVal == IDM_strVal
                                   select field_records.MIP_strVal).FirstOrDefault();
                if (Mapperfiled != null)
                {
                    return Mapperfiled.ToString();
                }
                return "";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                       // log.Info("MIP_strVal" + MIP_strVal + " + MIP_strVal + " + ve.ErrorMessage);
                    }
                }
                return null;
            }
        }
    }
}
