//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CDHCL2MIP.IDMDXchanger
{
    using System;
    
    public partial class usp_LAW_getprovisionaldates_Result
    {
        public string ledb_provpropstatus { get; set; }
        public string ledb_finalproposalstatus { get; set; }
        public Nullable<System.DateTime> ledb_finalpropsalsent { get; set; }
        public Nullable<System.DateTime> ledb_provproposalsent { get; set; }
    }
}
